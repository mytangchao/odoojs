# odoojs

2020-8-30  
之前使用 react 实现的  odoojs  
之后 使用 vue  




#### 介绍
odoojs 是一个 javascript 类库, 是前端访问 odoo 的接口.  
odoojs 是 odoo 在前端开发中的代言者.  
odoojs 的对外 api 非常友好. 使用 odoojs 如同在直接使用 odoo.  
换个说法, 习惯了的 odoo 的逻辑概念, 在 odoojs 中都可以看到.  


