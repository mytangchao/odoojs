import api from '@/api'
import formMixin1 from '@/mixins/formMixin1'

export default {
  mixins: [formMixin1],

  data() {
    return {
      // 父表数据加载完成后, 通知 子表 可以刷新 fields_get
      loaded: 0,
      onchanged: 0
    }
  },

  computed: {
    cardForView() {
      const card = this.metadata.cardForView || {}
      return {
        title: card.title
          ? card.title(this.dataDict)
          : this.dataDict.display_name
      }
    }
  },

  methods: {
    async init() {
      const moduleName = this.$route.meta.name
      this.moduleName = moduleName
      // console.log('init,', this.$route)
      this.modelName = this.$route.meta.model
      this.metadata = { ...api.env(this.modelName).metadata }
      await this.init_fields_get()

      const rid = this.$route.query.id
      if (rid) {
        await this.handleEdit(rid)
      } else {
        await this.handleNew2()
      }
      this.loaded = this.loaded + 1
    },

    async init_fields_get() {
      const fieldsGet = await this.api_fields_get()
      this.set_fieldsGet(fieldsGet)
    },

    async handleNew2() {
      const query = this.$route.query || {}
      const context2 = query.context
      const context = context2 ? JSON.parse(context2) : {}

      await this.handleNew({ context })
    },

    async handleChange2OfO2m(field, val, o2mformData) {
      console.log('handleChange2OfO2m', field, val, o2mformData)
      const values = { [field]: val }
      const formData = { [field]: o2mformData }

      this.values = { ...this.values, ...values }
      this.formData = { ...this.formData, ...formData }
      this._handleChange(field)
    },
    async handleChange2(field, val, ext = {}) {
      await this.handleChange(field, val, ext)
    },

    async handleEdit(rid) {
      const model = this.modelName
      const fieldsGet = await this.api_fields_get()
      this.set_fieldsGet(fieldsGet)

      const record = await api.env(model).browse_one(rid)

      // 取 o2m 数据
      this.dataDict = { ...record }
      const record2 = this.dataDict2formData(record, this.fieldsGet)
      // console.log('handleEdit fieldsGet', this.modelName, fieldsGet)
      // console.log('handleEdit formData', this.modelName, record2)

      this.formData = { ...record2 }
      this.values = {}
    },

    getReadonly(col) {
      // console.log('get readonly,', col, name)
      if (col.compute) {
        return true
      } else if (col.readonly && typeof col.readonly === 'function') {
        return col.readonly(this.dataDict)
      } else {
        return col.readonly
      }
    },

    // 表单页面 提交按钮, 创建记录或编辑记录, 完成后 刷新 list
    async handleSubmit() {
      // console.log('submit,', this.formData, this.values)
      const isEdit = this.formData.id
      if (isEdit) {
        await api.env(this.modelName).write(isEdit, this.values)
        this.$router.back()
      } else {
        const result = await api.env(this.modelName).create(this.values)
        this.$router.replace({
          path: this.$route.meta.paths.view,
          query: { id: result.id }
        })
      }
    }
  }
}
