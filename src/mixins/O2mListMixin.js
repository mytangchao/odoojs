import api from '@/api'

function toPrice(num) {
  num = parseFloat(num)
    .toFixed(2)
    .toString()
    .split('.')
  num[0] = num[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), '$1,')
  return '￥' + num.join('.')
}

export default {
  props: {
    odooModel: { type: String, default: '' },
    title: { type: String, default: '' },
    ids: { type: Array, default: () => [] }
  },

  data() {
    return {
      domain: {},

      modelName: undefined,
      metadata: {},
      dataList: []
    }
  },
  computed: {
    // listPage 用的
    // 移动端 app list 的 title / label / icon / value
    forAppList() {
      const meta = this.metadata.forAppList || {}
      return {
        title: rec => rec.display_name || rec.name || '',
        value: () => '',
        label: () => '',
        icon: () => '',
        ...meta
      }
    },

    // pc 控制列表页面的表格
    tabelForList() {
      const tableForList = this.metadata.tableForList || {}
      return {
        border: tableForList.border
      }
    },

    // pc 自动展示列表页面时, 需要的字段
    columnsForList() {
      return this.metadata.columnsForList
    },

    // pc 自动展示列表页面时, 需要的字段
    columnsForList2() {
      const cols = this.metadata.columnsForList
      const cols2 = this.columnsForTable(cols)

      return [
        // { title: 'ID', key: 'id' },
        // { type: 'index', width: 60, align: 'center' },
        ...cols2
        // {
        //   title: 'Action',
        //   slot: 'action',
        //   width: 200,
        //   align: 'center',
        //   fixed: 'right'
        // }
      ]
    }
  },

  watch: {
    ids: {
      handler: function(val) {
        this.handleSearch(val)
      },
      deep: true
    }
  },

  async created() {},

  methods: {
    columnsForTable(cols) {
      // console.log('columnsForTable, ', cols)
      // .filter(item => cols[item].type !== 'action')
      const cols2 = Object.keys(cols || {}).map(item => {
        const meta = cols[item]
        const item2 = {
          title: cols[item].label,
          key: item,
          width: cols[item].width,
          align: cols[item].align,
          tooltip: cols[item].tooltip
        }

        if (['select', 'radio'].includes(meta.type)) {
          item2.key = `${item}__name`
        }

        if (cols[item].type === 'img') {
          item2.render = (h, params) => {
            // console.log('table, img', h, params)
            return h('img', {
              /*  组件样式 */
              style: {
                width: '60%',
                height: '60%',
                'border-radius': '5%'
              },
              /*  html属性 */
              attrs: {
                /*  图片的路径,直接采用后台返回的键值 */
                src: params.row[`${item}__url`]
              }
            })
          }
        }

        if (cols[item].type === 'monetary') {
          item2.align = 'right'
          item2.render = (h, params) => {
            const val = params.row[params.column.key]
            const val2 = toPrice(val)
            return h('span', {}, val2)
          }
        }

        // if (cols[item].render) {
        //   item2.render = cols[item].render
        // }

        // if (cols[item].type === 'date') {
        //   // item2.slot = cols[item].slot || 'time'
        //   item2.render = (h, params) => {
        //     const time2 = params.row[params.column.key]
        //     const time3 = time2 && time2.slice(0, 10)
        //     return h('span', {}, time3)
        //   }
        // }

        return item2
      })

      // console.log('columnsForList2, isForm, cols', cols)

      return cols2
    },

    init() {
      this.moduleName = api.model_odoo2vuex(this.odooModel)
      this.modelName = this.odooModel
      this.metadata = { ...api.env(this.modelName).metadata }
      this.handleSearch()
    },

    async handleSearch(val) {
      const model = this.modelName
      const ids = val || this.ids

      if (ids.length) {
        const res = await api.env(model).web_search_read({
          domain: { id: ids }
        })

        this.dataList = [...res.records]
      }
    }
  }
}
