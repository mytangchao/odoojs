import api from '@/api'
import formMixin1 from '@/mixins/formMixin1'

export default {
  mixins: [formMixin1],
  props: {
    parentLoaded: { type: Number, default: 0 },
    parentOnchanged: { type: Number, default: 0 },

    parentField: { type: String, default: '' },
    refField: { type: String, default: '' },
    label: { type: String, default: '' },
    odooModel: { type: String, default: '' },
    readonly: { type: Boolean, default: false },
    data: { type: Array, default: () => [] },

    parentformData: {
      // parent-form-data
      type: Object,
      default: () => {
        return {}
      }
    },

    parentData: {
      type: Object,
      default: () => {
        return {}
      }
    }
  },

  data() {
    return {
      formList: [],
      dataId: 0,
      isSaved: 0
    }
  },

  computed: {
    dataList() {
      const dataList = this.formList.filter(line => line.op !== 2)
      return dataList.map(line => {
        if (line.dataDict.id) {
          return { ...line.dataDict }
        } else {
          return { id: line.id, ...line.dataDict }
        }
      })
    }
  },

  watch: {
    parentLoaded(/*val*/) {
      // 父表 数据更新完成了, 才可以设置子表
      this.init()
    }
    // parentOnchanged(val) {
    //   // 父表 数据更新完成 可能带回了 子表的数据
    //   this.onchangeOfParent(val)
    // }
  },

  async created() {
    this.modelName = this.odooModel
    this.metadata = { ...api.env(this.modelName).metadata }
    await this.init_fields_get()
  },

  methods: {
    async init() {
      if (this.data.length) {
        // 这是编辑过来的 数据
        const dataList = await api.env(this.modelName).browse(this.data)
        // console.log('o2m dataList', this.odooModel, dataList)
        this.formList = dataList.map(item => {
          return {
            op: 4,
            id: item.id,
            values: {},
            formData: this.dataDict2formData(item, this.fieldsGet),
            dataDict: { ...item }
          }
        })
      }
    },

    // onchangeOfParent(val) {
    //   //
    //   // console.log('onchange of parent data', this.parentData)
    //   // console.log('onchangeparent form', this.parentformData)
    //   // const parentData = JSON.parse(JSON.stringify(this.parentData))
    //   // const parentformData = JSON.parse(JSON.stringify(this.parentformData))
    //   // console.log('onchg dataDict', parentData)
    //   // console.log('onchg formData', parentformData)
    //   // console.log('onchange of parent', val, this.parentField)
    // },
    async init_fields_get() {
      const fieldsGet = await this.api_fields_get()
      this.set_fieldsGet(fieldsGet)
    },

    async handleO2mNew() {
      console.log('p data,', this.refField, this.parentformData)
      const values = { [this.refField]: this.parentformData }
      await this.handleNew({ values })
      this.formData = { ...this.formData, ...values }

      const get_new_o2m_id = () => {
        const ids = this.formList.map(line => line.id)
        const min_id = ids.length ? Math.min(...ids) : 0
        return (min_id < 0 ? min_id : 0) - 1
      }
      this.dataId = get_new_o2m_id()
    },

    async handleO2mChange(field, val, ext = {}) {
      console.log(' handleO2mChange,', field, val, typeof val, ext)

      await this.handleChange(field, val, ext)

      if (this.isSaved) {
        this._save_formList()
        this.isSaved = 0
      }
    },

    async handleEdit(row) {
      // console.log('edit', row)
      const lines = this.formList.filter(item => item.id === row.id)
      // console.log('xxx, o2m_edit 2', lines)

      if (!lines.length) {
        return
      }
      const line = lines[0]

      if (![0, 1, 4].includes(line.op)) {
        return
      }

      this.dataId = line.id
      this.dataDict = { ...line.dataDict }
      this.formData = { ...line.formData, [this.refField]: this.parentformData }
      this.values = { ...line.values }

      if (line.fieldsGet) {
        const fgs = Object.keys(line.fieldsGet).reduce((acc, cur) => {
          acc[cur] = { ...line.fieldsGet[cur] }
          return acc
        }, {})
        this.fieldsGet = fgs
      } else {
        const fieldsGet = await this.api_fields_get({ page: 'o2mEdit' })
        this.set_fieldsGet(fieldsGet)
      }
    },
    async handleDel(row) {
      const formList = this.formList.reduce((acc, line) => {
        if (line.id !== row.id) {
          acc.push(line)
        } else if (line.op !== 0) {
          acc.push({ op: 2, id: line.id })
        }
        return acc
      }, [])
      this.formList = [...formList]
      this._submit()
    },

    _save_formList() {
      console.log(' _save_formList')
      const o2m_id = this.dataId
      const dataDict = { ...this.dataDict }
      const formData = { ...this.formData }
      delete formData[this.refField]
      const values = { ...this.values }

      const fieldsGet = Object.keys(this.fieldsGet).reduce((acc, cur) => {
        acc[cur] = { ...this.fieldsGet[cur] }
        return acc
      }, {})

      const newline = { id: o2m_id, dataDict, formData, values, fieldsGet }

      const get_formList = () => {
        const lines = this.formList.filter(line => line.id === o2m_id)
        if (lines.length) {
          return this.formList.map(line => {
            if (line.id !== o2m_id) {
              return line
            } else {
              return { op: line.op === 4 ? 1 : line.op, ...newline }
            }
          })
        } else {
          return [...this.formList, { op: 0, ...newline }]
        }
      }

      const formList = get_formList()
      console.log('_save_formList', formList)
      this.formList = [...formList]
    },

    async submit() {
      console.log(' submit')
      this._save_formList()
      this.isSaved = 1
      this._submit()
    },

    async _submit() {
      const forms = this.formList.map(ln => {
        return [ln.op, ln.id, [1, 0].includes(ln.op) ? ln.formData : null]
      })

      const vals = this.formList.map(ln => {
        return [ln.op, ln.id, [1, 0].includes(ln.op) ? ln.values : null]
      })

      // console.log(' submit,', lines)
      this.$emit('change', this.parentField, vals, forms)
    }
  }
}
