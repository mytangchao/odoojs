import api from '@/api'

function toPrice(num) {
  num = parseFloat(num)
    .toFixed(2)
    .toString()
    .split('.')
  num[0] = num[0].replace(new RegExp('(\\d)(?=(\\d{3})+$)', 'ig'), '$1,')
  return '￥' + num.join('.')
}

export default {
  data() {
    return {
      modelName: undefined,
      metadata: {},
      dataDict: {}
    }
  },
  computed: {
    cardForView() {
      const card = this.metadata.cardForView || {}
      return {
        title: card.title
          ? card.title(this.dataDict)
          : this.dataDict.display_name
      }
    },
    // 控制 新增按钮,  显示文本 / 是否隐藏
    btnNew() {
      const btn = this.metadata.btnNew || {}
      btn.label = btn.label || '新增'
      return btn
    },

    hasBtnNew() {
      const btnHidden = this.btnNew.hidden
      return !(btnHidden && typeof btnHidden === 'function'
        ? btnHidden(this.dataList)
        : btnHidden)
    },

    btnEdit() {
      const btn = this.metadata.btnEdit || {}
      btn.label = btn.label || '编辑'
      return btn
    },

    hasBtnEdit() {
      const btnHidden = this.btnEdit.hidden
      return !(btnHidden && typeof btnHidden === 'function'
        ? btnHidden(this.dataDict)
        : btnHidden)
    },

    // 控制 删除按钮,  显示文本 / 是否隐藏
    btnDel() {
      const btn = this.metadata.btnDel || {}
      btn.label = btn.label || '删除'
      return btn
    },

    hasBtnDel() {
      const btnHidden = this.btnDel.hidden
      return !(btnHidden && typeof btnHidden === 'function'
        ? btnHidden(this.dataDict)
        : btnHidden)
    },

    // 自动展示详情页面时, 需要的字段
    columnsForView() {
      return this.metadata.columnsForView
    },

    // 自动展示详情页面时, wkf
    workflow() {
      return this.metadata.workflow
    },

    wkfCurrentStep() {
      // console.log('this', this)
      const workflow = this.workflow || {}
      const steps = this.wkfSteps
      const state = workflow.state
        ? workflow.state(this.dataDict)
        : this.dataDict.state
      return Object.keys(steps).findIndex(item => item === state)
    },

    wkfSteps() {
      return this.filterWorkflow((this.workflow || {}).steps)
    },

    wkfButtons() {
      return this.filterWorkflow((this.workflow || {}).buttons)
    },

    wkfState() {
      const steps = this.filterWorkflow((this.workflow || {}).steps)
      // console.log('steps', steps)
      const label = (Object.values(steps)[this.wkfCurrentStep] || {}).string
      const s2 = !label
        ? ''
        : typeof label === 'function'
        ? label(this.dataDict)
        : label
      return s2
    }
  },

  watch: {},

  async created() {},

  methods: {
    // 点击新增按钮, 跳转表单页面
    async onBtnNew() {
      // console.log('onBtnNew,where_from', where_from)
      this.$router.push({ path: this.$route.meta.paths.form })
    },

    // 点击编辑按钮, 跳转表单页面
    async onBtnEdit(rec) {
      // console.log('onBtnEdit', rec, where_from)
      this.$router.push({
        path: this.$route.meta.paths.form,
        query: { id: rec.id }
      })
    },

    // 点击删除按钮, 执行删除操作,
    // 可以重写覆盖 该函数, 增加 确认对话框, 在确认后, call this.handleDel(rec)
    onBtnDel(rec) {
      this.handleDel(rec.id)
    },

    async init() {
      const moduleName = this.$route.meta.name
      this.moduleName = moduleName
      this.modelName = this.$route.meta.model

      this.metadata = { ...api.env(this.modelName).metadata }
      const rid = this.$route.query.id
      if (rid) {
        const model = this.modelName
        const res = await api.env(model).browse_one(rid)
        this.dataDict = { ...res }
      }
    },

    filterWorkflow(allData = {}) {
      // console.log('filterWorkflow.', allData, this.dataDict)

      const btns = Object.keys(allData).reduce((acc, cur) => {
        const hidden = allData[cur].hidden
        const hidden2 =
          hidden && typeof hidden === 'function'
            ? hidden(this.dataDict)
            : hidden
        if (!hidden2) {
          acc[cur] = allData[cur]
        }

        return acc
      }, {})

      return btns
    },

    async handleDel(rid) {
      // const res =
      await api.env(this.modelName).unlink(rid)
    },

    async handleWorkflow(name) {
      const rid = this.dataDict.id
      if (rid) {
        console.log('handleWorkflow 1,', name)
        const model = this.modelName
        const res = await api.env(model).workflow(rid, name, this.dataDict)
        const { record } = res
        if (record) {
          this.dataDict = { ...record }
        }
        return res
      }
    },

    async onClickWkf2Btn(name, { line_field, line_values }) {
      // console.log('onClickWkf2Btn in mixin', name, line_field, line_values)
      this.handleWorkflow2({ name, line_field, line_values })
    },

    handleWorkflow2({ name, line_field, line_values }) {
      // console.log(name, line_field, line_values)
      const rid = this.dataDict.id
      if (rid) {
        api.env(this.modelName).workflow2(rid, name, {
          values: this.dataDict,
          line_field,
          line_values
        })
        // 执行之后 什么时候刷新的页面?
        // 在 app 代码中 调试下
      }
    },

    toPrice(num) {
      return toPrice(num)
    },

    // viewUI 使用, view page 显示 列表 子表数据
    getColumnsForTable(col /*, cname */) {
      //
      const cols = Object.keys(col.columns || {}).reduce((acc, cur) => {
        if (col.columns[cur].type !== 'action') {
          acc[cur] = col.columns[cur]
        }
        return acc
      }, {})

      //

      const cols2 = this.columnsForTable(cols)

      const that = this

      const cols3 = Object.keys(col.columns || {})
        .filter(item => col.columns[item].type === 'action')
        .map(item => {
          const item2 = col.columns[item]
          // console.log('item 2, ', item2)

          const btns = item2.buttons

          return {
            key: 'action',
            width: item2.width,
            align: 'center',
            render(h, params) {
              // eslint-disable-next-line no-unused-vars
              const { row, column } = params

              // const btns = item2.buttons

              const btns2 = Object.keys(btns).map(btn => {
                const b = btns[btn]
                // console.log('click action , before,', that, row, column)

                return h(
                  'Button',
                  {
                    props: { icon: b.icon, size: 'small' },
                    on: {
                      click() {
                        that.onClickWkf2Btn(btn, {
                          message: b.message,
                          line_field: col.data,
                          line_values: row
                        })
                      }
                    }
                  },
                  b.label
                )
              })

              return h('span', btns2)
            }
          }
        })

      return [...cols3, ...cols2]
    },
    columnsForTable(cols) {
      // console.log('columnsForTable, ', cols)
      // .filter(item => cols[item].type !== 'action')
      const cols2 = Object.keys(cols || {}).map(item => {
        const meta = cols[item]
        const item2 = {
          title: cols[item].label,
          key: item,
          width: cols[item].width,
          align: cols[item].align,
          tooltip: cols[item].tooltip
        }

        if (['select', 'radio'].includes(meta.type)) {
          item2.key = `${item}__name`
        }

        if (cols[item].type === 'img') {
          item2.render = (h, params) => {
            // console.log('table, img', h, params)
            return h('img', {
              /*  组件样式 */
              style: {
                width: '60%',
                height: '60%',
                'border-radius': '5%'
              },
              /*  html属性 */
              attrs: {
                /*  图片的路径,直接采用后台返回的键值 */
                src: params.row[`${item}__url`]
              }
            })
          }
        }

        if (cols[item].type === 'monetary') {
          item2.align = 'right'
          item2.render = (h, params) => {
            const val = params.row[params.column.key]
            const val2 = toPrice(val)
            return h('span', {}, val2)
          }
        }

        // if (cols[item].render) {
        //   item2.render = cols[item].render
        // }

        // if (cols[item].type === 'date') {
        //   // item2.slot = cols[item].slot || 'time'
        //   item2.render = (h, params) => {
        //     const time2 = params.row[params.column.key]
        //     const time3 = time2 && time2.slice(0, 10)
        //     return h('span', {}, time3)
        //   }
        // }

        return item2
      })

      // console.log('columnsForList2, isForm, cols', cols)

      return cols2
    }
  }
}
