import api from '@/api'

export default {
  data() {
    return {
      modelName: undefined,
      metadata: {},
      fieldsGet: {},
      dataDict: {},
      formData: {},
      values: {}
    }
  },

  computed: {
    columnsForForm() {
      // console.log('columnsForForm', this.metadata)
      return (this.metadata || {}).columnsForForm
    }
  },

  methods: {
    dataDict2formData(record, fieldsGet) {
      return Object.keys(record).reduce((acc, cur) => {
        if (cur.split('__').length > 1 && cur.split('__')[0]) {
          return acc
        }

        const meta = fieldsGet[cur] || {}
        if (['one2many', 'many2many'].includes(meta.type)) {
          const ids = record[cur]
          acc[cur] = ids.map(item => [4, item, null])
          return acc
        }

        acc[cur] = record[cur]
        return acc
      }, {})
    },

    // select 字段 获取 options
    getSelectOptins(field) {
      const meta = this.fieldsGet[field] || {}
      // console.log(
      //   'getSelectOptins .fieldsGet',
      //   this.modelName,
      //   field,
      //   this.fieldsGet
      // )
      return meta.selection || []
    },

    fields_get_checkcb(fld) {
      const meta = this.fieldsGet[fld]
      return meta && meta._set
    },
    fields_get_callback(fld, ops) {
      const meta = this.fieldsGet[fld] || {}
      const meta2 = { ...meta, selection: ops, _set: 1 }
      this.fieldsGet = { ...this.fieldsGet, [fld]: meta2 }
      // console.log(
      //   'api_fields_get,callback',
      //   this.modelName,
      //   fld,
      //   ops,
      //   this.fieldsGet
      // )
    },
    async api_fields_get(payload = {}) {
      const checkcb = fld => this.fields_get_checkcb(fld)
      const callback = (fld, ops) => this.fields_get_callback(fld, ops)

      const model = this.modelName
      const fieldsGet = await api
        .env(model)
        .fields_get({ ...payload, callback, checkcb })
      return fieldsGet
    },

    async api_onchange(payload = {}) {
      const checkcb = fld => this.fields_get_checkcb(fld)
      const callback = (fld, ops) => this.fields_get_callback(fld, ops)

      // console.log('api_onchange', this.modelName, this.fieldsGet)

      const model = this.modelName
      const result = await api
        .env(model)
        .onchange({ ...payload, fieldsGet: this.fieldsGet, callback, checkcb })
      return result
    },

    set_fieldsGet(fields) {
      const fieldsGet = Object.keys(fields).reduce((acc, cur) => {
        const meta = this.fieldsGet[cur] || {}
        acc[cur] = { ...meta, ...fields[cur] }
        return acc
      }, {})
      this.fieldsGet = { ...this.fieldsGet, ...fieldsGet }
    },

    async handleNew(paylaod = {}) {
      const result = await this.api_onchange(paylaod)
      const { value, value_form, value_form2, fields } = result

      this.set_fieldsGet(fields)

      this.dataDict = { ...value }
      this.formData = { ...value_form }
      this.values = { ...value_form2 }
    },
    async handleChange(field, val, ext = {}) {
      // console.log('handleChange', this.modelName, field, val)
      const values = { [field]: val }
      this.values = { ...this.values, ...values }
      this.formData = { ...this.formData, ...values }
      const dataDict = { ...values, ...ext }
      this.dataDict = { ...this.dataDict, ...dataDict }
      await this._handleChange(field)
    },

    async _handleChange(field) {
      const values2 = { ...this.formData }
      delete values2.id
      const ids = this.dataDict.id > 0 ? [this.dataDict.id] : []

      const payload = { ids, values: values2, field_name: field }
      // console.log('_handleChange,0', this.model, this.dataDict.id)
      // console.log('_handleChange,1', this.model, payload, this.values)

      const result = await this.api_onchange(payload)
      const { value, value_form, value_form2, fields } = result

      this.set_fieldsGet(fields)
      // console.log('_handleChange 2,', this.model, value_form)

      this.dataDict = { ...this.dataDict, ...value }
      this.formData = { ...this.formData, ...value_form }
      this.values = { ...this.values, ...value_form2 }
    }
  }
}
