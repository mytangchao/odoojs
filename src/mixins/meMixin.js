export default {
  data() {
    return {}
  },
  computed: {
    uid: function() {
      return this.$store.state.user.uid
    },

    session: function() {
      // console.log(this.$store.state.user)
      return this.$store.state.user.session_info
    },

    partner_id: function() {
      return this.session.partner_id
    },

    username: function() {
      return this.session.username
    },

    user_companies() {
      return this.session.user_companies || {}
    },

    allowed_companies() {
      return this.user_companies.allowed_companies
    },
    current_company() {
      return this.user_companies.current_company || [0, '']
    },

    company_info() {
      return this.session.company_info
    }
  },

  watch: {},

  async created() {},

  methods: {}
}
