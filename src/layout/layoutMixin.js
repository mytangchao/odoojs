export default {
  data() {
    return {
      fullWidth: document.documentElement.clientWidth
    }
  },
  computed: {},
  created() {
    window.addEventListener('resize', this.handleResize)
  },
  beforeDestroy: function() {
    window.removeEventListener('resize', this.handleResize)
  },

  methods: {
    handleResize() {
      this.fullWidth = document.documentElement.clientWidth
    }
  }
}
