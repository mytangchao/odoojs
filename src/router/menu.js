import api from '@/api'

/*
  这里是把 api 里的所有 model 全部定义了 路由
*/

export const get_menu = () => {
  const addons_menus = Object.keys(api.addons)
  // console.log('get_menu', addons_menus)
  return addons_menus.reduce((acc, item) => {
    const name = item
      .replace(/(\.|^)[a-z]/g, L => L.toUpperCase())
      .replace(/\./g, '')
      .replace(/( |^)[A-Z]/g, L => L.toLowerCase())

    const addon = api.addons[item]
    const title = (addon.metadata || {}).description
    const isMenu = (addon.metadata || {})._isMenu
    if (isMenu > 0) {
      acc.push({ name, title, model: item, type: 'menu' })
    }

    const isReport = (addon.metadata || {})._isReport
    if (isReport && isReport > 0) {
      acc.push({ name, title, model: item, type: 'report' })
    }

    return acc
  }, [])
}

// export const get_menu2 = apptype => {
//   const pathOfViews = apptype === 'app' ? 'appui' : 'pcui'

//   const addons_menus = Object.keys(api.addons)
//   // console.log('get_menu', addons_menus)

//   return addons_menus.map(item => {
//     const name = item
//       .replace(/(\.|^)[a-z]/g, L => L.toUpperCase())
//       .replace(/\./g, '')
//       .replace(/( |^)[A-Z]/g, L => L.toLowerCase())

//     const addon = api.addons[item]
//     const title = (addon.metadata || {}).description

//     // console.log('menu,', item, addon._isMenu, addon)

//     const ret = {
//       name, // 这是 $store.moduleName
//       title: title, // 这是菜单名
//       model: item // 这是 api.modelName
//     }

//     const isMenu = (addon.metadata || {})._isMenu
//     // const isMenu = isMenu1 === undefined ? 1 : isMenu1
//     if (isMenu > 0) {
//       ret.files = {
//         list: `${pathOfViews}/listPage`, // 这是 导入的组件文件 @/views/odoo/listPage.vue
//         view: `${pathOfViews}/viewPage`,
//         form: `${pathOfViews}/formPage`
//       }
//       ret.paths = {
//         list: `/${pathOfViews}/list/${name}`, // 这是 路由
//         view: `/${pathOfViews}/view/${name}`,
//         form: `/${pathOfViews}/form/${name}`
//       }
//     }

//     const isReport = (addon.metadata || {})._isReport

//     if (isReport && isReport > 0) {
//       // console.log('menu,isReport,', name)
//       ret.files = {
//         list: `${pathOfViews}/reportList`, // 这是 导入的组件文件 @/views/odoo/listPage.vue
//         view: `${pathOfViews}/reportView`
//         // report: `${pathOfViews}/report/${name}`
//       }

//       ret.paths = {
//         list: `/${pathOfViews}/list/${name}`,
//         view: `/${pathOfViews}/view/${name}`
//         // report: `/${pathOfViews}/report/${name}`
//       }

//       if (apptype === 'app') {
//         ret.files.report = `${pathOfViews}/report/${name}`
//         ret.paths.report = `/${pathOfViews}/report/${name}`
//       }
//     }

//     return ret
//   })
// }
