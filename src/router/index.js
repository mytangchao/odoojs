import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import { isApp } from '@/config/index'

console.log('isApp', isApp)

/* Layout */
import LayoutSpace from '@/layout/space'
import LayoutPc from '@/layout/pcui'

import {
  get_menu
  // get_menu2
} from './menu'
const _import = require('./import.js')

const Layout = isApp ? LayoutSpace : LayoutPc

const get_homeRoutes = () => {
  const redirect = isApp ? '/home/company' : '/home'
  const component = () =>
    isApp ? import('@/views/app/home/home') : import('@/views/pc/home')

  const children = isApp
    ? [
        {
          path: '/home/home2',
          component: () => import('@/views/app/home/home2'),
          name: 'home-home2'
        },
        {
          path: '/home/company',
          component: () => import('@/views/app/home/company'),
          name: 'home-company'
        },
        {
          path: '/home/company/sub',
          component: () => import('@/views/app/home/company-menu/sale'),
          name: 'home-company-sub'
        },
        {
          path: '/home/message',
          component: () => import('@/views/app/home/message'),
          name: 'home-message'
        },
        {
          path: '/home/me',
          component: () => import('@/views/app/home/me'),
          name: 'home-me'
        }
      ]
    : undefined

  return [
    {
      path: '/',
      component: Layout,
      redirect,
      children: [
        {
          path: '/home',
          component,
          name: 'home',
          children
        }
      ]
    }
  ]
}

const userRoutes = [
  // user
  {
    path: '/user',
    component: LayoutSpace,
    children: [
      {
        path: '/user/login',
        component: () =>
          isApp ? import('@/views/app/user') : import('@/views/pc/user'),
        name: 'user-login'
      },
      {
        path: '/user/resetpsw',
        component: () =>
          isApp
            ? import('@/views/app/user/resetPsw')
            : import('@/views/pc/user/resetPsw'),
        name: 'user-reset'
      },
      {
        path: '/user/register',
        component: () =>
          isApp
            ? import('@/views/app/user/register')
            : import('@/views/pc/user/register'),
        name: 'user-reg'
      }
    ]
  }
]

const homeRoutes = get_homeRoutes()

const modelMenus = get_menu()

const get_modelRoutes = ({ type, models, isReport }) => {
  const get_fileName = (type, name) => {
    const files = {
      list: isApp
        ? isReport
          ? 'app/report/reportList'
          : 'app/list/listPage'
        : isReport
        ? 'pc/report/reportList'
        : 'pc/list/listPage',
      view: isApp
        ? isReport
          ? `app/report/${name}`
          : 'app/view/viewPage'
        : isReport
        ? `pc/report/reportView`
        : 'pc/view/viewPage',
      form: isApp ? 'app/form/formPage' : 'pc/form/formPage'
    }
    return files[type]
  }

  const models2 = models.filter(
    item => item.type === (isReport ? 'report' : 'menu')
  )

  return [
    {
      path: `/${type}`,
      component: Layout,
      children: models2.map(item => {
        const { model, name } = item
        const fileName = get_fileName(type, name)
        const meta = {
          model,
          name,
          paths: {
            list: `/list/${name}`,
            view: `/view/${name}`,
            form: `/form/${name}`
          }
        }

        return {
          path: `/${type}/${name}`,
          component: _import(fileName) || null,
          name: `${type}-${name}`,
          meta
        }
      })
    }
  ]
}

const viewRoutes = get_modelRoutes({ type: 'view', models: modelMenus })
const formRoutes = get_modelRoutes({ type: 'form', models: modelMenus })
const viewReportRoutes = get_modelRoutes({
  type: 'view',
  isReport: 1,
  models: modelMenus
})
const listRoutes = get_modelRoutes({ type: 'list', models: modelMenus })
const listReportRoutes = get_modelRoutes({
  type: 'list',
  isReport: 1,
  models: modelMenus
})

const allRoutes = [
  ...userRoutes,
  ...homeRoutes,
  ...listRoutes,
  ...viewRoutes,
  ...formRoutes,
  ...listReportRoutes,
  ...viewReportRoutes
]

// 创建路由
const createRouter = () => {
  const routers = [...allRoutes]

  return new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: routers
  })
}

const router = createRouter()

// // Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// export function resetRouter() {
//   const newRouter = createRouter()
//   router.matcher = newRouter.matcher // reset router
// }

export default router
