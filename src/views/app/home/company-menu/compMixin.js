export default {
  name: 'CompanyMixin',
  components: {},
  mixins: [],

  data() {
    return {
      menus: {
        sale: {
          icon: 'hot-sale-o',
          text: '销售',
          children: {
            saleOrder: {},
            resPartnerCompany: {},
            productTemplate: {},
            accountMoveInvoiceOut: {},
            accountPaymentCustomerIn: {}
          }
        },
        purchase: {
          icon: 'shopping-cart-o',
          text: '采购',
          children: {
            purchaseOrder: {},
            resPartnerCompany: { title: '供应商' },
            productTemplate: {},
            accountMoveInvoiceIn: {},
            accountPaymentSupplierOut: {}
          }
        },
        // stock: { icon: 'diamond-o', text: '库存' },
        cash: {
          icon: 'gold-coin-o',
          text: '出纳',
          children: {
            resPartnerCompany: { title: '业务伙伴' },
            productTemplate: {},
            accountMoveInvoiceOut: {},
            accountMoveInvoiceIn: {},
            accountPaymentCustomerIn: {},
            accountPaymentSupplierOut: {},
            accountPaymentTransfer: {}
          }
        },
        account: {
          icon: 'balance-list-o',
          text: '会计',
          children: {
            resPartnerCompany: { title: '业务伙伴' },
            productTemplate: {},
            accountMoveInvoiceOut: {},
            accountMoveInvoiceIn: {},
            accountMoveEntry: {},
            accountMoveOpening: {}
          }
        },

        accountPosted: {
          icon: 'balance-list-o',
          text: '财务查询',
          children: {
            accountAccount: {},
            accountMove: {},
            accountMoveLinePosted: {}
            // accountReportGeneralLedger: {},
            // accountReportPartnerLedger: {},
            // accountingReportBalancesheet: {},
            // accountingReportProfitandloss: {}
          }
        },
        accountReport: {
          icon: 'balance-list-o',
          text: '财务报表',

          children: {
            accountBalanceReport: {},
            accountReportGeneralLedger: {},
            accountReportPartnerLedger: {},
            accountingReportBalancesheet: {},
            accountingReportProfitandloss: {}
          }
        }
        // hr: { icon: 'friends-o', text: 'HR' },
        // hr_expense: { icon: 'balance-pay-o', text: '费用报销' },
      },

      menus_report: {},

      subMenus: {
        saleOrder: { title: '销售订单' },
        purchaseOrder: { title: '采购订单' },
        resPartnerCompany: { title: '客户' },
        productTemplate: { title: '产品' },
        accountMoveInvoiceOut: { title: '销售账单' },
        accountMoveInvoiceIn: { title: '采购账单' },
        accountPaymentCustomerIn: { title: '收款单' },
        accountPaymentSupplierOut: { title: '付款单' },
        accountPaymentTransfer: { title: '内部转账' },
        accountMoveEntry: { title: '凭证录入' },
        accountMoveOpening: { title: '期初设置' },
        accountMove: { title: '凭证查询' },
        accountMoveLinePosted: { title: '分录查询' },
        accountAccount: { title: '科目查询' },

        accountBalanceReport: { title: '试算平衡', type: 'account_report' },
        accountReportGeneralLedger: { title: '总账', type: 'account_report' },
        accountReportPartnerLedger: {
          title: '应收应付账',
          type: 'account_report'
        },
        accountingReportBalancesheet: {
          title: '余额表',
          type: 'account_report'
        },
        accountingReportProfitandloss: {
          title: '损益表',
          type: 'account_report'
        }
      }
    }
  },

  computed: {
    // menus() {
    //   return {
    //   }
    // }
  },

  async created() {},

  methods: {
    //
  }
}
