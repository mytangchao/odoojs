// 本文件是 odoojs 的入口文件

import {
  model_odoo2vuex as tools_model_odoo2vuex,
  model_vuex2odoo as tools_model_vuex2odoo
} from './utils'
// rpc 中为访问网络的最基础的接口
import rpcCreator from './rpc.js'
import { OdooBus } from './rpc.js'

// ModelClass 封装了 模型 模型的 CRUD 操作
import { ModelClass } from './models'

// import addons2 from './addons/accountPayment'

const get_addons = () => {
  // 导入 ./addons 下定义的 所有 models
  // 1. addons model : 指 addons 中的 model
  // 2. odoo model: 指 服务端 odoo 中的 model
  // 3. addons model == odoo model,
  // 4. 是对 odoo model 的 feilds_get 的 重复描述, 以便于前端处理
  // 5. 可以 多个 addons model 对应 同一个 odoo model
  // 6. addons 中 未定义的 odoo model, 可以使用, 只是 feilds_get 只有 [id, display_name ]
  // 7. addons 中 可以自己继承, 继承意味着 对应同一个 odoo model

  const odooAddonsFiles = require.context('./addons', true, /\.js$/)

  // 这个格式, 文件名 不再是 模型名
  const my_addons = odooAddonsFiles.keys().reduce((models, modulePath) => {
    // const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
    const value = odooAddonsFiles(modulePath)
    models = { ...models, ...value.default }
    return models
  }, {})

  // 这个格式 是 以 文件名作为 模型名, 现在放弃这个方法
  // const my_addons = odooAddonsFiles.keys().reduce((models, modulePath) => {
  //   // set './app.js' => 'app'
  //   const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  //   const value = odooAddonsFiles(modulePath)
  //   models[moduleName] = value.default
  //   return models
  // }, {})

  return my_addons
}

// 导入 model,
const odoojs_addons = get_addons()
// debug
// const odoojs_addons = addons2
// console.log('ODOO odoojs_addons, ', odoojs_addons)

// 将外部定义的模型 与 odoojs 中已有的 model 合并
const merge_addons = api_models => {
  // 重名的, 意味着 是 api_model 对 odoojs_addons 的扩展

  const api_models2 = Object.keys(api_models).reduce((acc, cur) => {
    const get_extend = (extend1, extend2) => {
      if (extend1 && extend2) {
        // 两个都有 extend, 则定义一个新函数, 实现继承
        return BaseClass => extend2(extend1(BaseClass))
      } else if (extend1 || extend2) {
        return extend1 || extend2
      } else {
        return undefined
      }
    }
    const model_in_addons = odoojs_addons[cur]

    if (model_in_addons) {
      // 外部与内部同名, 则
      // 1. merge metadata, 这种情况下,  应该 merge columns
      // 2. extend 是继承

      const model_in_api = api_models[cur]
      //
      const {
        _name: name1,
        _inherit: inherit1,
        metadata: metadata1 = {},
        extend: extend1
      } = model_in_addons

      const {
        _name: name2,
        _inherit: inherit2,
        metadata: metadata2 = {},
        extend: extend2
      } = model_in_api

      const _name = name2 || name1
      const _inherit = inherit2 || inherit1

      // 这样调用是为了处理自己的 fieldsForQuery
      const metadata = merge_metadate(
        merge_metadate({}, metadata1),
        metadata2,
        'merge_all'
      )
      const extend = get_extend(extend1, extend2)

      const new_model = { _name, _inherit, metadata, extend }
      acc[cur] = new_model
    } else {
      acc[cur] = api_models[cur]
    }

    return acc
  }, {})

  return { ...odoojs_addons, ...api_models2 }
}

export class ODOO {
  constructor(params) {
    const { error, models: api_models = {} } = params
    // 这个是 外部定义的  addons
    this.api_models = api_models

    // 这个是 odoojs 自己定义的 addons
    this.odoojs_addons = odoojs_addons

    // console.log('ODOO api_models,', api_models)

    // 这里 merge odoojs_addons 及 api_models
    this.addons = merge_addons(api_models)
    // 缺少 metadata 的 自动补一个? TBD
    // debug
    // this.addons = odoojs_addons
    console.log('ODOO addons,', this.addons)

    this.rpc = rpcCreator({ error })
    this.bus = OdooBus.getBus()
  }

  model_odoo2vuex(name) {
    return tools_model_odoo2vuex(name)
  }

  model_vuex2odoo(name) {
    return tools_model_vuex2odoo(name)
  }

  toVuex(name) {
    return tools_model_odoo2vuex(name)
  }

  toModel(name) {
    return tools_model_vuex2odoo(name)
  }

  // Not Used
  ref(xmlid) {
    // get model and id from xmlid
    return this.env('ir.model.data').call('xmlid_to_res_model_res_id', [
      xmlid,
      true
    ])
  }

  get_userinfo() {
    return this.rpc.get_userinfo()
  }

  // merge_metadate(parent_src, metadata_src) {
  //   return merge_metadate(parent_src, metadata_src)
  // }

  // env 是最重要的函数, 取模型的入口
  env(model) {
    // console.log('xxx, env model,', model)
    const MyModelClass = this._get_env_class(model)
    const Model = new MyModelClass({
      model: this._get_env_model(model),
      metadata: this._get_env_metadata(model),
      odoo: this,
      rpc: this.rpc
    })

    // console.log('env, model,', model, Model)

    return Model
  }

  _get_env_class_inherits(_inherit) {
    const inherits = this._get_env_class_inherits_recursion(_inherit)
    const inherits2 = inherits.reverse().reduce((acc, cur) => {
      const index = acc.findIndex(item => item === cur)
      if (index < 0) {
        acc.push(cur)
      }
      return acc
    }, [])

    return inherits2
  }

  _get_env_class_inherits_recursion(_inherit) {
    if (!_inherit) {
      return []
    }
    const inherits2 = Array.isArray(_inherit) ? _inherit : [_inherit]

    const inherits3 = inherits2.reduce((acc, cur) => {
      acc.push(cur)
      const ma = this.addons[cur]
      if (ma) {
        const parent = this._get_env_class_inherits_recursion(ma._inherit)
        acc = [...acc, ...parent]
      }
      return acc
    }, [])

    return inherits3
  }

  _get_env_class(model) {
    // console.log('xxx, _get_env_class2,', model)
    const ma = this.addons[model]
    // console.log('xxx, _get_env_class2, ma', ma)
    if (!ma) {
      return ModelClass
    }
    // 找出所有的 祖先
    const inherits2 = this._get_env_class_inherits(ma._inherit)
    const all_models = [...inherits2, model]
    // console.log('xxx, _get_env_class2, all_models', all_models)
    const MyModelClass = all_models.reduce((acc, cur) => {
      const m2 = this.addons[cur]
      if (m2 && m2.extend) {
        acc = m2.extend(acc)
        // console.log('xxx, _get_env_class2, Cls ok', cur, acc)
      }
      return acc
    }, ModelClass)
    // const mm = new MyModelClass({})
    // console.log('xxx, _get_env_class2, mm', mm)
    return MyModelClass
  }

  // _get_env_class22(model) {
  //   // 搞定继承
  //   const ma = this.addons[model]
  //   if (!ma) {
  //     return ModelClass
  //   }

  //   // 如果有继承 则递归处理
  //   const MyModelClass = ma._inherit
  //     ? this._get_env_class(ma._inherit)
  //     : ModelClass
  //   return ma.extend ? ma.extend(MyModelClass) : MyModelClass
  // }

  _get_env_model(model) {
    // console.log('get mdoel,', model)
    const ma = this.addons[model]
    if (!ma) {
      return model
    }

    if (ma._name) {
      return ma._name
    }

    const inherits = this._get_env_class_inherits(ma._inherit).reverse()
    // console.log('xxx, get mdoel, inherits', inherits)

    const parent_model = inherits.reduce(
      (acc, cur) => {
        const Model = this.addons[cur]
        if (acc[0]) {
          return acc
        }

        if (Model._name) {
          acc[0] = Model._name
        } else {
          acc[1] = cur
        }
        return acc
      },
      [null, null]
    )

    const name = parent_model[0] || parent_model[1] || model

    // const name = inherits.length ? inherits[0] : model
    // console.log('xxx, get mdoel, name', name)
    return name
  }

  _get_env_metadata(model) {
    const metadata = this._get_env_metadata2(model)
    const default_fields = { display_name: null }
    const default_columns = { display_name: { label: '名称' } }
    const metadata2 = { description: '', ...metadata }
    metadata2.fieldsForSearch = metadata.fieldsForSearch || default_fields
    metadata2.fieldsForBrowse = metadata.fieldsForBrowse || default_fields
    // metadata2.fieldsForEdit = metadata.fieldsForEdit || default_fields
    metadata2.columnsForList = metadata.columnsForList || default_columns
    metadata2.columnsForView = metadata.columnsForView || default_columns

    // 处理 columnsForForm, columnsForList, columnsForView
    // 定义 columnsForPage 为对象, 包括所有的 列
    // columnsForXXX 中 只定义 字段名, 具体内容从 columnsForPage 取
    // 如果 columnsForXXX 中 自己定义额外属性, 则 取自己的
    //
    const colsForPage = metadata2.columnsForPage

    const colsForList = merge_dict(colsForPage, toDict(metadata.columnsForList))
    const colsForView = merge_dict(colsForPage, toDict(metadata.columnsForView))
    const colsForForm = merge_dict(colsForPage, toDict(metadata.columnsForForm))

    const metadata_columns = {
      columnsForList: filterDict(colsForList, metadata.columnsForList),
      columnsForView: filterDict(colsForView, metadata.columnsForView),
      columnsForForm: filterDict(colsForForm, metadata.columnsForForm)
    }

    return { ...metadata2, ...metadata_columns }
  }

  _get_env_metadata2(model) {
    const ma = this.addons[model]
    if (!ma) {
      return {}
    }

    // 找出所有的 祖先
    const inherits2 = this._get_env_class_inherits(ma._inherit)
    const all_models = [...inherits2, model]
    // console.log('xxx, _get_env_metadata2, all_models', all_models)

    const matadata = all_models.reduce((acc, cur) => {
      const mm = this.addons[cur]
      if (mm && mm.metadata) {
        acc = merge_metadate(acc, mm.metadata)
      }
      // console.log('xxx, _get_env_metadata2, matadata for:', cur, acc)
      return acc
    }, {})
    // console.log('xxx, _get_env_metadata2, matadata', matadata)

    return matadata
  }
}

const merge_metadate = (parent_src, metadata_src, merge_all) => {
  // fieldsForXxx 相似度太高了
  // 所以提供一个 fieldsForQuery, 以一抵三

  const parent = { ...parent_src }
  const metadata = { ...metadata_src }

  // fieldsForQuery 定义了 哪些字段做查询
  // fieldsForEdit 不再有用了
  // fieldsForEdit 定义了 哪些字段可编辑, 不继承 fieldsForQuery
  // fieldsForSearch 和 fieldsForBrowse 是 search 和 browse 的参数
  // fieldsForSearch 和 fieldsForBrowse 可以继承 fieldsForQuery

  const fq = metadata.fieldsForQuery
  // metadata.fieldsForSearch = metadata.fieldsForSearch || fq
  // metadata.fieldsForBrowse = metadata.fieldsForBrowse || fq
  metadata.fieldsForSearch = merge_dict(fq, metadata.fieldsForSearch)
  metadata.fieldsForBrowse = merge_dict(fq, metadata.fieldsForBrowse)
  // metadata.fieldsForEdit = metadata.fieldsForEdit || fq

  const parent2 = { ...parent }
  const metadata2 = { ...metadata }

  const no_merge_meta2 = {}
  if (!merge_all) {
    // 如果 被 merge_addons 调用, 则 merge columns
    // 这些配置, 做覆盖操作, 这是自动生成页面用的
    const no_merge_meta = ['columnsForForm', 'columnsForList', 'columnsForView']
    no_merge_meta.forEach(item => {
      no_merge_meta2[item] = metadata[item] || parent[item]
      delete parent2[item]
      delete metadata2[item]
    })
  }

  // merge 两个 dict
  const metadate_new = merge_dict(parent2, metadata2)

  return { ...metadate_new, ...no_merge_meta2 }
}

const toDict = a => {
  const b = a || {}
  return Object.keys(b).reduce((acc, cur) => {
    acc[cur] = b[cur] || {}
    return acc
  }, {})
}

const filterDict = (src = {}, dst = {}) => {
  // console.log('xxx, cols,', src, dst)
  return Object.keys(dst).reduce((acc, cur) => {
    acc[cur] = src[cur]
    return acc
  }, {})
}

const isDict = a => {
  if (a === null) {
    return false
  }
  return typeof a === 'object' && !Array.isArray(a)
}

const merge_dict = (dict1 = {}, dict2 = {}) => {
  // 取出第一层的所有 key, 去重复
  // console.log('------', dict1, dict2)
  const keys = [...new Set([...Object.keys(dict1), ...Object.keys(dict2)])]

  return keys.reduce((acc, cur) => {
    const value1 = dict1[cur]
    const value2 = dict2[cur]

    if (value1 !== undefined && value2 !== undefined) {
      if (isDict(value1) && isDict(value2)) {
        // 都是字典 做 递归处理
        acc[cur] = merge_dict(value1, value2)
      } else {
        // 至少有一个不是字典, 直接取第二个
        acc[cur] = value2
      }
    } else {
      if (value2 !== undefined) {
        acc[cur] = value2
      } else if (value1 !== undefined) {
        acc[cur] = value1
      }
    }

    return acc
  }, {})
}
