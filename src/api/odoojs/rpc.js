import requestCreator from './request'
import { getToken, setToken, removeToken } from '@/utils/auth'
// 2020-10-24 TBD 这个三个东西 没用了 , 最后 排查下 清理掉

// // 登录时 需要 数据库名称
const OdooDatabase = process.env.VUE_APP_ODOO_DB
const busPoll = process.env.VUE_APP_ODOO_BUS_POLL === 'true'

// 登录成功后, 本地存储 user info
const UserInfoKey = 'UserInfo'

const PollUrl = '/longpolling/poll'

const SessionCheckUrl = '/web2/session/check'
const LoginUrl = '/web2/session/authenticate'
const GetSessionInfoUrl = '/web2/session/get_session_info'
const LogoutUrl = '/web2/session/destroy'
const CallUrl = '/web2/dataset/call_kw'
const WebSearchRead = '/web2/dataset/search_read'

const AuthCodeSendUrl = '/web2/auth/codesend'
const AuthCodeBackUrl = '/web2/auth/codeback'
const AuthExistUrl = '/web2/auth/exist'
const AuthLoginUrl = '/web2/auth/login'
const AuthResetPswUrl = '/web2/auth/resetpsw'
const AuthRegisterUrl = '/web2/auth/register'

const my_return = res => {
  return new Promise((resolve, reject) => {
    try {
      setTimeout(resolve, 100, res)
    } catch {
      reject()
    }
  })
}

export class OdooBus {
  //
  constructor() {
    this.instance = null
  }

  static getBus() {
    if (!this.instance) {
      this.instance = new OdooBus()
      this.instance.init()
    }
    return this.instance
  }

  init() {
    this.isRun = 0
    this.loop = 1
    this.last = 0

    this.channels = []
    const error = () => {
      setTimeout(() => {
        this.poll()
      }, 1000)
    }

    this.rpc = new OdooRpc({ error, timeout: 100000 })

    // this.poll()
  }

  setCallback(channel_id, cb) {
    const chn = this.findOrCreateChannel(channel_id)
    chn.callback = cb
  }

  findOrCreate(records, vals) {
    const recs = records.filter(item => item.id === vals.id)
    if (recs.length) {
      return recs[0]
    }
    records.push(vals)
    return vals
  }

  findOrCreateChannel(chn_id) {
    return this.findOrCreate(this.channels, {
      callback: null,
      id: chn_id,
      partner_ids: [],
      messages: []
    })
  }

  updateInsert(records, vals) {
    const rec = this.findOrCreate(records, { id: vals.id })
    Object.keys(vals).forEach(item => {
      rec[item] = vals[item]
    })
  }

  updateChannel(chn) {
    // console.log('xxxxx, updateChannel,', chn)
    const chn0 = this.findOrCreateChannel(chn.id)
    // console.log('xxxxx, updateChannel 2,', chn0)
    if (chn.partner_id) {
      this.updateInsert(chn0.partner_ids, chn.partner_id)
    }

    chn0.messages.push(...chn.messages)
  }

  async poll() {
    this.isRun = 1
    console.log('xxxxx, bus start poll')
    while (this.loop) {
      console.log('xxxxx, bus runing poll', this.last)
      const res = await this.rpc.poll(this.last)
      const msgIds = res.map(item => item.id)
      if (msgIds.length) {
        this.last = Math.max(...msgIds)
      }
      res.forEach(element => {
        console.log('xxx, poll,', element)
        const chn = {
          id: element.channel[2],
          partner_id: null,
          messages: []
        }
        const msg = element.message

        if (msg.info === 'typing_status') {
          chn.partner_id = {
            id: msg.partner_id,
            is_typing: msg.is_typing
          }
        } else if (msg.info === 'channel_fetched') {
          chn.partner_id = {
            id: msg.partner_id,
            is_typing: false,
            last_message_id: msg.last_message_id
          }
        } else if (msg.id) {
          chn.messages = [msg]
        }

        // console.log('xxx, poll,', chn, this.channels)
        this.updateChannel(chn)
        const goodchn = this.findOrCreateChannel(chn.id)

        if (goodchn.callback) {
          goodchn.callback(goodchn)
        }

        // console.log('xxx, poll 2,', this.channels)
        // console.log('xxx, poll 3,', this.channels[0].partner_ids[0])
      })
    }
    console.log('xxxxx, bus stop poll')
    this.isRun = 0
  }

  startPoll() {
    this.loop = 1
    if (!this.isRun) {
      this.poll()
      //
    }
  }

  stopPoll() {
    this.loop = 0
  }

  getMsg(channel_id) {
    return this.findOrCreateChannel(channel_id)
  }
}

class OdooRpcBase {
  constructor(params) {
    // const { error, timeout,  } = params
    this.request = requestCreator(params)
    this.exportRequest = requestCreator({ ...params, requestType: 'export' })
    this.importRequest = requestCreator({ ...params, requestType: 'import' })

    // this.bus = new OdooBus(params)
    this.longpoll = requestCreator({ ...params, timeout: 100000 })
  }
  async poll(last) {
    // console.log('odoorpc.poll:', last)
    const res = await this.longpoll({
      url: `${PollUrl}`,
      method: 'post',
      data: { channels: [], last: last || 0 }
    })
    // console.log('odoorpc.poll:', res)

    return res
  }

  switchBus(openClose) {
    if (busPoll) {
      const bus = OdooBus.getBus()
      if (openClose) {
        bus.startPoll()
      } else {
        bus.stopPoll()
      }
    }
  }

  reset_token() {
    this.switchBus(false)
    removeToken()
    localStorage.clear(UserInfoKey)
    return my_return(true)
  }

  get_userinfo() {
    const userstr = localStorage.getItem(UserInfoKey)
    if (userstr) {
      const userinfo = JSON.parse(localStorage.getItem(UserInfoKey))
      return userinfo
    } else {
      return {}
    }
  }
  async login(params) {
    // console.log('odoorpc.user.login:', params)
    const { username, password } = params

    const userinfo = await this.request({
      url: LoginUrl,
      method: 'post',
      data: { login: username, password, db: OdooDatabase }
    })
    // console.log('odoorpc.user.login ok:', userinfo)

    return this.after_login(userinfo)
  }

  after_login(userinfo) {
    //
    // const userinfo = {
    //   session_id: 'token-test',
    //   name: username
    // }

    console.log('userinfo', userinfo)

    if (userinfo) {
      const token = userinfo.session_id
      setToken(token)

      localStorage.setItem(UserInfoKey, JSON.stringify(userinfo))
      this.switchBus(true)
    }
    return my_return({ token: (userinfo || {}).session_id })
  }

  async get_session_info() {
    const url = this.get_url(GetSessionInfoUrl)
    const userinfo = await this.request({ url, method: 'post' })

    if (userinfo) {
      localStorage.setItem(UserInfoKey, JSON.stringify(userinfo))
    }

    return my_return(userinfo)
  }

  get_url(url) {
    const token = getToken()
    const sid2 = token ? `?session_id=${token}` : ''
    return `${url}${sid2}`
  }

  async session_check() {
    const url = this.get_url(SessionCheckUrl)
    await this.request({ url, method: 'post' })
    return my_return(null)
  }

  async logout() {
    console.log('odoorpc.user.logout:')
    this.switchBus(false)

    const url = this.get_url(LogoutUrl)
    this.request({ url, method: 'post' })

    removeToken()
    localStorage.clear(UserInfoKey)

    return my_return(true)
  }

  async callWebSearchRead(model, kwargs = {}) {
    const url = this.get_url(WebSearchRead)

    const res = await this.request({
      url,
      method: 'post',
      data: { model, ...kwargs }
    })

    return res
  }

  async call(model, method, args = [], kwargs = {}) {
    // console.log('odoorpc.call:', model, method, args, kwargs)
    const url = this.get_url(`${CallUrl}/${model}/${method}`)
    const res = await this.request({
      url,
      method: 'post',
      data: { model, method, args, kwargs }
    })

    return res
  }

  async xlsx_export(data) {
    const url = '/web/export/xlsx'
    return this.base_export(url, data)
  }

  async csv_export(data) {
    const url = '/web/export/csv'
    return this.base_export(url, data)
  }

  async base_export(url1, data) {
    //
    const token = 'dummy-because-api-expects-one'
    const url = `${url1}`
    const userinfo = this.get_userinfo()

    const csrf_token = userinfo.csrf_token

    const res = await this.exportRequest({
      url,
      method: 'post',
      data: {
        data: JSON.stringify(data),
        token,
        csrf_token
      }
    })
    return res
  }

  async base_import_set_file({ import_id, file }) {
    //
    const url1 = '/base_import/set_file'
    const url = `${url1}`
    const userinfo = this.get_userinfo()

    const csrf_token = userinfo.csrf_token

    console.log('file', file)

    // const file_type = file.type
    // const file_name = file.name

    const res = await this.importRequest({
      url,
      method: 'post',
      data: { csrf_token, import_id, file }
    })
    return res
  }
}

export class OdooRpc extends OdooRpcBase {
  async auth_codesend({ authtype, account, method }) {
    // const { email, tosend } = payload
    // console.log('odoorpc.user.auth_code_send:', payload)

    const userinfo = await this.request({
      url: `${AuthCodeSendUrl}`,
      method: 'post',
      data: { db: OdooDatabase, authtype, account, method }
    })

    return my_return(userinfo)
  }

  async auth_codeback({ authtype, account, method }) {
    // console.log('odoorpc.user.auth_code_back:')
    // const { email } = payload
    const res = await this.request({
      url: `${AuthCodeBackUrl}`,
      method: 'post',
      data: { db: OdooDatabase, authtype, account, method }
    })

    // console.log('odoorpc.user.sms_back:', res)

    return my_return(res)
  }

  async auth_exist({ authtype, account, method }) {
    // console.log('auth_exist', authtype, account, method)
    const res = await this.request({
      url: AuthExistUrl,
      method: 'post',
      data: { authtype, account, method, db: OdooDatabase }
    })
    return my_return(res)
  }

  async auth_login({ authtype, account, code }) {
    // console.log('odoorpc.user.auth_login:')
    const userinfo = await this.request({
      url: `${AuthLoginUrl}`,
      method: 'post',
      data: { authtype, account, code, db: OdooDatabase }
    })
    // console.log('odoorpc.user.auth_login:', userinfo)

    return this.after_login(userinfo)
  }

  async auth_resetpsw({ authtype, account, code, password }) {
    // console.log('odoorpc.user.auth_reset_password:')
    const res = await this.request({
      url: `${AuthResetPswUrl}`,
      method: 'post',
      data: { authtype, account, code, password, db: OdooDatabase }
    })

    return my_return(res)
  }

  async auth_register({ authtype, account, code, login, password }) {
    // console.log('odoorpc.user.auth_register:')
    const res = await this.request({
      url: `${AuthRegisterUrl}`,
      method: 'post',
      data: { authtype, account, code, login, password, db: OdooDatabase }
    })

    return my_return(res)
  }
}

const rpcCreator = params => {
  return new OdooRpc(params)
}

export default rpcCreator
