const AccountPayment = {
  metadata: {
    description: '',

    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: false, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '编辑' },
    btnDel: { hidden: rec => rec.state !== 'draft', label: '删除' },

    fields: {
      // journal_id: { type: 'many2one' },
      // payment_method_id: { type: 'many2one' }
    },

    field_onchange_ref: 'account.view_account_payment_form',
    // field_onchange_model: 'account.payment',
    // field_onchange_field: '',

    fieldsForQuery: {
      payment_type: null,
      partner_type: null,
      payment_date: null,
      name: null,
      journal_id: null,
      partner_id: null,
      amount: null,
      state: null
    },

    fieldsForBrowse: {
      payment_type: null,
      partner_type: null,
      payment_date: null,
      name: null,
      journal_id: null,
      partner_id: null,
      amount: null,
      state: null,
      communication: null,
      reconciled_invoices_count: null,
      reconciled_invoice_ids: null,
      move_line_ids: {
        name: null,
        partner_id: null,
        account_id: null,
        debit: null,
        credit: null
      }
    },

    context: {
      // default_partner_type: 'customer',
      // default_payment_type: 'inbound',
      // res_partner_search_mode: 'customer'
    },

    workflow: {
      // 工作流的步骤
      steps: {
        draft: { string: '草稿' },
        posted: { string: '确认' },
        sent: { string: '发送', hidden: rec => rec.state !== 'sent' },
        // reconciled: { string: '核销' },
        cancelled: { string: '取消', hidden: rec => rec.state !== 'cancelled' } // 仅在 取消后显示
      },
      buttons: {
        post: {
          type: 'object',
          string: '确认',
          message: '确认?',
          btn_type: 'primary',
          hidden: rec => !['draft'].includes(rec.state)
        },

        search_invoice: {
          type: 'router',
          string: '查看销售账单',
          message: '查看销售账单?',
          hidden: rec => rec.reconciled_invoices_count === 0,
          path: '/list/AccountMoveInvoiceOut',
          domain(rec) {
            return {
              id: rec.reconciled_invoice_ids
            }
          }
        },

        cancel: {
          type: 'object',
          string: '取消',
          message: '取消?',
          hidden: rec => !['draft'].includes(rec.state)
        },
        action_draft: {
          type: 'object',
          string: '重置为草稿',
          message: '重置为草稿?',
          hidden: rec => rec.state === 'draft'
        }
      }
    },

    columnsForPage: {
      payment_type: { type: 'select', label: '收付方向' },
      partner_type: { type: 'select', label: '客户 or 供应商' },
      payment_date: { type: 'date', label: '日期' },
      name: { label: '号码' },
      journal_id: { type: 'select', label: '账簿' },
      destination_journal_id: { type: 'select', label: '转账到' },

      partner_id: { type: 'select', label: '业务伙伴' },
      amount: { type: 'monetary', label: '金额' },
      state: { type: 'select', label: '状态' },
      communication: { label: '凭证编号' },
      reconciled_invoices_count: { label: '发票数' },
      reconciled_invoice_ids: { label: '发票ids' },

      move_line_ids: {
        type: 'table',
        label: '会计分录',
        hidden: rec => rec.state === 'draft',
        odooModel: 'account.move.line.entry', //
        vuxModel: 'accountMoveLineEntry'
      }
    },

    columnsForList: {
      payment_type: null,
      payment_date: null,
      name: null,
      journal_id: null,
      partner_id: null,
      amount: null,
      state: null
    },

    columnsForView: {
      // name: null,
      payment_type: null,
      // partner_type: null,
      payment_date: null,
      journal_id: null,
      partner_id: null,
      amount: null,
      communication: null,
      move_line_ids: null
      // state: null,
      // reconciled_invoices_count: null,
      // reconciled_invoice_ids: null,
      // move_line_ids: {}
    },

    columnsForForm: {
      partner_id: null,
      journal_id: null,
      amount: null,
      payment_date: null,
      communication: null
    }

    //
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      // async default_get(payload) {
      //   const res = await super.default_get(payload)
      //   // console.log('default get', res)
      //   return { ...res, amount: 0.0 }
      // }
    }

    return ModelClass
  }
}

const AccountPaymentCustomer = {
  _inherit: 'account.payment',
  metadata: {
    description: '',
    context: {
      default_partner_type: 'customer',
      res_partner_search_mode: 'customer'
    },

    workflow: {
      buttons: {
        search_invoice: {
          type: 'router',
          string: '查看销售账单',
          message: '查看销售账单?',
          hidden: rec => rec.reconciled_invoices_count === 0,
          path: '/list/AccountMoveInvoiceOut',
          domain(rec) {
            return {
              id: rec.reconciled_invoice_ids
            }
          }
        }
      }
    },

    columnsForPage: {
      // payment_type: { type: 'select', label: '收付' },
      // partner_type: { type: 'select', label: '客户22' },
      partner_id: { type: 'select', label: '客户' }
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, partner_type: 'customer' }
        })
      }
    }
    return ModelClass
  }
}

const AccountPaymentSupplier = {
  _inherit: 'account.payment',

  metadata: {
    description: '',

    context: {
      default_partner_type: 'supplier',
      res_partner_search_mode: 'supplier'
    },

    workflow: {
      buttons: {
        search_invoice: {
          type: 'router',
          string: '查看采购账单',
          message: '查看采购账单?',
          hidden: rec => rec.reconciled_invoices_count === 0,
          path: '/list/AccountMoveInvoiceIn',
          domain(rec) {
            return {
              id: rec.reconciled_invoice_ids
            }
          }
        }
      }
    },

    columnsForPage: {
      // payment_type: { type: 'select', label: '收付' },
      // partner_type: { type: 'select', label: '供应商' },
      partner_id: { type: 'select', label: '供应商' }
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, partner_type: 'supplier' }
        })
      }
    }
    return ModelClass
  }
}

const AccountPaymentIn = {
  _inherit: 'account.payment',
  metadata: {
    description: '',
    context: {
      default_payment_type: 'inbound'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, payment_type: 'inbound' }
        })
      }
    }
    return ModelClass
  }
}

const AccountPaymentOut = {
  _inherit: 'account.payment',
  metadata: {
    description: '',
    context: {
      default_payment_type: 'outbound'
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, payment_type: 'outbound' }
        })
      }
    }
    return ModelClass
  }
}

const AccountPaymentCustomerIn = {
  _inherit: ['account.payment.customer', 'account.payment.in'],
  metadata: {
    _isMenu: 1 // 控制菜单用的,
  }
}

const AccountPaymentCustomerOut = {
  _inherit: ['account.payment.customer', 'account.payment.out'],
  metadata: {
    _isMenu: 1 // 控制菜单用的,
  }
}

const AccountPaymentSupplierOut = {
  _inherit: ['account.payment.supplier', 'account.payment.out'],
  metadata: {
    _isMenu: 1 // 控制菜单用的,
  }
}

const AccountPaymentSupplierIn = {
  _inherit: ['account.payment.supplier', 'account.payment.in'],
  metadata: {
    _isMenu: 1 // 控制菜单用的,
  }
}

const AccountPaymentTransfer = {
  _inherit: 'account.payment',
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',
    context: {
      default_payment_type: 'transfer'
    },
    workflow: {
      // 工作流的步骤
      buttons: {
        search_invoice: {
          hidden: true
        }
      }
    },

    columnsForPage: {
      // journal_id: { type: 'select', label: '账簿' },
      // destination_journal_id: { type: 'select', label: '转账到' }
    },

    columnsForForm: {
      journal_id: null,
      destination_journal_id: null,
      amount: null,
      payment_date: null,
      communication: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, payment_type: 'transfer' }
        })
      }
    }
    return ModelClass
  }
}

const Models = {
  'account.payment': AccountPayment,
  'account.payment.customer': AccountPaymentCustomer,
  'account.payment.supplier': AccountPaymentSupplier,
  'account.payment.in': AccountPaymentIn,
  'account.payment.out': AccountPaymentOut,
  'account.payment.customer.in': AccountPaymentCustomerIn,
  'account.payment.customer.out': AccountPaymentCustomerOut,
  'account.payment.supplier.out': AccountPaymentSupplierOut,
  'account.payment.supplier.in': AccountPaymentSupplierIn,
  'account.payment.transfer': AccountPaymentTransfer
}

export default Models
