const ResPartner = {
  metadata: {
    description: '参与人',

    context: {
      default_company_id: self => {
        const company_id = self.get_company_id()
        return company_id
      }
    },

    // 这里定义基本的字段属性 可被其他地方利用
    // 目前实现了:
    // 1 图片字段的处理, saerch/browse 后, 生成 图片字段的 url
    fields: {
      // image_1920: { type: 'image' },
      // image_1024: { type: 'image' },
      // image_512: { type: 'image' },
      // image_256: { type: 'image' },
      image_128: { type: 'image' },
      date: { type: 'date' },
      type: { type: 'selection', string: '地址类型' }
    },

    // field_onchange_ref: 'base.view_partner_form',
    // 使用上面的 form , onchange 会出错, 原因不明
    field_onchange_ref: 'base.view_partner_short_form',
    // field_onchange_model: 'res.partner',
    // field_onchange_field: '',

    //
    fieldsForQuery: {
      name: null,
      email: null,
      ref: null,
      category_id: null,
      date: null,
      type: null,
      comment: null,
      company_id: null
    },

    fieldsForSearch: undefined,
    fieldsForBrowse: undefined,

    importTemplatePath: '/base_import_patch/static/xls/res_partner.xls',

    import_export: {
      // model: 'res.partner',
      fields: [
        'name',
        'is_company',
        'parent_id',
        'type',
        'street',
        'zip',
        'city',
        'state_id',
        'country_id'
      ],

      name_create_enabled_fields: {},

      fields_export: [
        { name: 'id', label: '外部ID' },
        { name: 'phone', label: '电话' },
        { name: 'company_type', label: '公司类别' },
        { name: 'email', label: 'Email' },
        { name: 'user_id', label: '销售员' },
        { name: 'activity_ids', label: '活动' },
        { name: 'city', label: '城市' },
        { name: 'country_id', label: '国家' },
        { name: 'company_id', label: '公司' }
      ]
    },

    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: false, label: '查看' },
    btnEdit: { hidden: false, label: '编辑' },
    btnDel: { hidden: false, label: '删除' },

    // 这个是模仿 odoo 到搜索框, 暂时不用这办法了,
    // 2020-9-30, 保留, 若以后启用这办法, 再议
    searchs: {
      type: {
        string: '个人或公司',
        filters: {
          type_person: { string: '个人', domain: [['is_company', '=', false]] },
          type_company: { string: '公司', domain: [['is_company', '=', true]] }
        }
      },
      sale_or_purchase: {
        string: '客户或供应商',
        filters: {
          customer: { string: '客户', domain: [['customer_rank', '>', 0]] },
          supplier: { string: '供应商', domain: [['supplier_rank', '>', 0]] },
          inactive: { string: '已归档', domain: [['active', '=', false]] }
        }
      }
    },

    // 移动端用, 暂时 保留
    forAppList: {
      title: rec => {
        return `${rec.display_name}`
      },
      icon: rec => {
        return `${rec.image_128__url}`
        // get_image('res.partner', rec.id, 'image_1920')
      },

      label: rec => {
        return `${rec.email || ''}`
      },
      value: rec => {
        return `${rec.email || ''}`
      }
    },

    workflow: {
      buttons: {
        search_sale_order: {
          type: 'router',
          string: '销售订单',
          path: '/list/SaleOrder',
          domain(rec) {
            return {
              partner_id___child_of: rec.id
            }
          }
        },

        search_purchase_order: {
          type: 'router',
          string: '采购订单',
          path: '/list/PurchaseOrder',
          domain(rec) {
            return {
              partner_id___child_of: rec.id
            }
          }
        },

        search_invoice_out: {
          type: 'router',
          string: '销售账单',
          path: '/list/AccountMoveInvoiceOut',
          domain(rec) {
            return {
              partner_id___child_of: rec.id,
              type: ['out_invoice', 'out_refund'],
              invoice_payment_state: 'not_paid',
              state: 'posted'
            }
          }
        },
        search_invoice_in: {
          type: 'router',
          string: '采购账单',
          path: '/list/AccountMoveInvoiceIn',
          domain(rec) {
            return {
              partner_id___child_of: rec.id,
              type: ['in_invoice', 'in_refund']
            }
          }
        }
      }
    },

    // page 各个字段的统一定义, 包括 list view 和 form
    columnsForPage: {
      image_128__url: { type: 'img', label: '图片' },
      image_128: { type: 'img', label: '图片' },
      name: { label: '名称' },
      ref: { label: '内部编码' },

      email: { label: '邮箱' },
      category_id: {
        type: 'select',
        multiple: 'multiple',
        label: '标签',
        placeholder: '标签'
      },
      date: { type: 'date', label: '日期' },
      type: { type: 'radio', label: '类型' },
      company_id: { type: 'select', label: '类型' },
      image_1920: { type: 'img', url: 'image_128__url', label: '图片' },
      comment: { type: 'textarea', label: '备注', placeholder: '备注' }
    },

    columnsForList: {
      image_128: null,
      name: null,
      ref: null,
      type: null,
      company_id: null
    },

    columnsForView: {
      name: null,
      ref: null,
      category_id: null,
      date: null,
      type: null,
      image_128: null
    },

    columnsForForm: {
      name: null,
      email: null,
      category_id: null,
      date: null,
      type: null,
      image_1920: null,
      comment: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      get_globals_dict({ values = {}, globals_dict = {} }) {
        const globals_dict2 = { ...globals_dict }
        if (values.country_id) {
          globals_dict2.country_id = values.country_id
        }
        return super.get_globals_dict({ values, globals_dict: globals_dict2 })
      }

      main_partner() {
        return this.call('main_partner')
      }

      find_or_create(email) {
        return this.call('find_or_create', [email])
      }
    }
    return ModelClass
  }
}

const ResPartnerEmployee = {
  _inherit: 'res.partner', // 继承自 哪个 js model. 若为空, 本身就是 根 model, 直接对应 odoo model
  // _name: 'res.partner', // 对应 odoo model, 若为空, 则依次取 [ 父 js model 的 _name, 自己的 env参数名 ]

  metadata: {
    description: '参与人 员工',
    filter: ['sale_or_purchase.customer']
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, employee: true }
        })
      }

      async create(values = {}, kwargs = {}) {
        const values2 = { ...values, employee: true }
        return super.create(values2, kwargs)
      }
    }
    return ModelClass
  }
}

const ResPartnerCompany = {
  _inherit: 'res.partner',

  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '公司',

    context: {
      default_customer_rank: 1,
      default_supplier_rank: 1,
      default_is_company: true
    },

    columnsForList: {
      name: null,
      ref: null,
      image_128: null
      // type: null,
      // company_id: null
    },

    columnsForView: {
      name: null,
      ref: null,
      image_128: null
      // type: null,
      // company_id: null
    },
    columnsForForm: {
      name: null,
      ref: null,
      image_1920: null
      // type: null,
      // company_id: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {}
    return ModelClass
  }
}

const ResPartnerPerson = {
  _inherit: 'res.partner',

  metadata: {
    description: '个人'
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {}
    return ModelClass
  }
}

const Models = {
  'res.partner': ResPartner,
  'res.partner.employee': ResPartnerEmployee,
  'res.partner.company': ResPartnerCompany,
  'res.partner.person': ResPartnerPerson
}

export default Models
