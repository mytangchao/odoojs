const ResUsers = {
  metadata: {
    description: ''
  },
  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async change_password(oldPsw, newPsw) {
        const method = 'change_password'
        const args = [oldPsw, newPsw]
        const res = await this.call(method, args)
        return res
      }

      async check_company_name(name) {
        const comp = await this.env('res.company').search_one({
          domain: { name }
        })

        if (comp.id) {
          return false
        } else {
          return true
        }

        // return name !== '333' ? true : false
      }

      async chech_reg_company(name) {
        // SO 销售订单
        // AI 销售发票
        // -0 has_company: 有公司 无需注册
        // -0 to_rename: !so.id && name 重名
        // -0 to_reg: !so.id
        // 0 cancel: so.state=draft or cancel: to 恢复订单
        // 1 draft: so.state=draft or cancel: to 确认订单
        // 2 to_invoice: so.state=sale && invoice_status='to invoice' :  to 发票
        // 3 invoicing: so.state=sale && invoice_status='invoiced'
        //              && ai.state=draft: to 确认发票
        // 4 in_payment: so.state=sale && invoice_status='invoiced'
        //              && ai.state=posted && ai.invoice_payment_state=in_payment: to 收款单
        // 5 paid: so.state=sale && invoice_status='invoiced'
        //              && ai.state=posted && ai.invoice_payment_state=paid:  todo
        // 6 todo: 其他 未知情况

        const session = this.get_userinfo()
        const { partner_id, company_info = {} } = session
        const { me_company_id } = company_info

        const SO = this.env('sale.order')
        const context = { allowed_company_ids: [1] }
        const domain = { partner_id, origin: 'reg_company' }
        const so = await SO.search_one({ domain, context })

        if (me_company_id) {
          // 有公司 无需注册公司
          const company_name = me_company_id[1]
          if (!so.id) {
            return { state2: 'has_company_done', company_name }
          }
          const so_ret = {
            id: so.id,
            so_id: so.id,
            order_line: so.order_line,
            company_name: so.client_order_ref,
            state: so.state,
            invoice_status: so.invoice_status,
            invoice_ids: so.invoice_ids
          }

          if (so.state === 'done') {
            return { ...so_ret, state2: 'has_company_done', company_name }
          } else if (so.state === 'sale') {
            return { ...so_ret, state2: 'has_company_sale', company_name }
          } else {
            return { ...so_ret, state2: 'has_company_done', company_name }
          }
        }
        // 无公司
        // 判断 订单

        if (!so.id) {
          if (!name) {
            return { state2: 'to_reg', company_name: '' }
          }
          const comp = await this.env('res.company').search_one({
            domain: { name }
          })

          if (comp.id) {
            return { state2: 'to_rename', company_name: name }
          } else {
            return { state2: 'to_reg', company_name: '' }
          }
        }

        const so_ret = {
          id: so.id,
          so_id: so.id,
          order_line: so.order_line,
          company_name: so.client_order_ref,
          state: so.state,
          invoice_status: so.invoice_status,
          invoice_ids: so.invoice_ids
        }

        if (['cancel', 'draft'].includes(so.state)) {
          // 判断重名,
          const comp = await this.env('res.company').search_one({
            domain: { name: so.client_order_ref }
          })

          if (comp.id) {
            return {
              ...so_ret,
              state2: 'to_rename',
              company_name: so.client_order_ref
            }
          }

          // 没重名, 可以提交
          const PRD = this.env('product.product')
          const prds = await PRD.search({
            domain: { name___ilike: 'wx-' },
            context
          })
          // console.log('prd,', prds)
          return { ...so_ret, state2: so.state, products: prds }
        } else if (so.state === 'sale' && so.invoice_status === 'to invoice') {
          return { ...so_ret, state2: so.state }
        } else if (so.state === 'sale' && so.invoice_status === 'invoiced') {
          const AI = this.env('account.move')
          const ai = await AI.browse_one(so.invoice_ids, { context })

          if (ai.state === 'cancel') {
            return { ...so_ret, state2: 'invoicing0', invoice_state: ai.state }
          } else if (ai.state === 'draft') {
            return { ...so_ret, state2: 'invoicing', invoice_state: ai.state }
          } else if (ai.state === 'posted') {
            return {
              ...so_ret,
              state2: ai.invoice_payment_state,
              invoice_state: ai.state,
              invoice_payment_state: ai.invoice_payment_state
            }
          } else {
            return {
              ...so_ret,
              state2: 'todo',
              invoice_state: ai.state,
              invoice_payment_state: ai.invoice_payment_state
            }
          }
        } else if (['done'].includes(so.state)) {
          return { state2: so.state }
        } else {
          return { state2: 'todo' }
        }
      }

      async close_reg_company(payload) {
        const so = { ...payload }

        const SO = this.env('sale.order')
        const context = { allowed_company_ids: [1] }

        if (so.id) {
          if (so.state === 'sale') {
            let so2 = await SO.call('action_done', [so.id], { context })
            return so2
          }
        }
      }

      async create_company(payload) {
        const { company_name } = payload
        const session = this.get_userinfo()
        const { uid } = session
        return this.call('create_company', [uid, company_name])
      }

      async reg_company_pay(payload) {
        const { order_line, price_unit } = payload
        const so = { ...payload }

        const SO = this.env('sale.order')
        const context = { allowed_company_ids: [1] }
        if (so.id) {
          if (so.state === 'draft') {
            const vals = {
              order_line: [[1, order_line[0], { price_unit }]]
            }
            await SO.write(so.id, vals, { context })
            let so2 = await SO.call('action_confirm', [so.id], { context })
            so2 = await SO.workflow_wizard(so.id, 'create_invoice', { context })
            return so2
          }
        }
      }

      async reg_company(payload) {
        const { company_name } = payload
        const so = { ...payload }

        const SO = this.env('sale.order')
        const context = { allowed_company_ids: [1] }

        if (!so.id) {
          const session = this.get_userinfo()
          const { partner_id } = session

          const Prd = this.env('product.product')
          const domain = { name: 'reg_company' }
          const prd = await Prd.search_one({ domain, context })

          const values = {
            partner_id: partner_id,
            origin: 'reg_company',
            client_order_ref: company_name,
            order_line: [[0, 0, { product_id: prd.id }]]
          }
          const so2 = await SO.create(values, { context })
          return so2.id ? true : false
        } else if (so.state === 'cancel') {
          await SO.call('action_draft', [so.id], { context })
          await SO.write(so.id, { client_order_ref: company_name }, { context })
          return true
        } else if (so.state === 'draft') {
          await SO.write(so.id, { client_order_ref: company_name }, { context })
          return true
        } else {
          return false
        }
      }
    }
    return ModelClass
  }
}

const Models = {
  'res.users': ResUsers
}

export default Models
