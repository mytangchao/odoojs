const _date_fn = () => {
  const str00 = value => value.toString().padStart(2, '0')
  const date2str = date => {
    const yyyy = str00(date.getFullYear())
    const mm = str00(date.getMonth() + 1)
    const dd = str00(date.getDate())
    return `${yyyy}-${mm}-${dd}`
  }

  const date2str_month = date => {
    const yyyy = str00(date.getFullYear())
    const mm = str00(date.getMonth() + 1)
    // const dd = str00(date.getDate())
    return `${yyyy}年${mm}月`
  }

  const first_date = date => new Date(date.getFullYear(), date.getMonth(), 1)
  const date_add = (date, day) =>
    new Date(date.getTime() + 1000 * 60 * 60 * 24 * day)

  const all_months = date => {
    const month_first = first_date(new Date(date))
    const month_last = date_add(first_date(new Date()), 1)

    const years = Array.from(new Array(100).keys()).map(item => item + 1970)
    const months = Array.from(new Array(12).keys()).map(item => item)

    const months2 = years.reduce((acc, cur) => {
      months.forEach(item => {
        acc.push(new Date(cur, item, 1, 12, 0, 0))
        // acc.push(`${cur}-${str00(item)}-01`)
      })

      return acc
    }, [])

    const months3 = months2.filter(
      item => item >= month_first && item <= month_last
    )
    const months4 = months3.map(item => {
      return {
        id: item.getFullYear() * 100 + item.getMonth() + 1,
        date: date2str(item),
        date_month: date2str_month(item)
      }
    })
    return months4.reverse()
  }

  return {
    first_date,
    date_add,
    date2str,
    date2str_month,
    all_months
  }
}

const date_fn = _date_fn()

const AccountReportBase = {
  metadata: {
    description: ''
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async search_report_month_list() {
        const res = await this.env('account.move.line').search_one({
          limit: 1,
          fields: { date: null },
          order: 'date'
        })

        // console.log(res)
        const date = res.date
        // const date = '2018-08-22'

        const months = date_fn.all_months(date)
        // console.log(months)

        return months
      }

      //   async report_print(payload = {}) {
      //     const res = await super.report_print(payload)
      //     console.log('res 1:', res)
      //     //   const { data = {}, context = {}, Accounts = [] } = res
      //     //   const { date_from, date_to } = data
      //     //   const { report_type } = context
      //     //   const res2 = { date_from, date_to, report_type, Accounts }
      //     return res
      //   }

      test_report() {
        //
        // this.test_report1()
        // this.test_report2()
      }
      async test_report1() {
        const res = await this.report_month({ date_month: '2020-10-1' })
        console.log('res:', res)
      }

      async test_report2() {
        //
        const values = await this.default_get_onchange()
        const date_from = '2020-1-1'
        const date_to = '2020-10-10'
        const values_input = { date_from: date_from, date_to: date_to }
        const values2 = { ...values, ...values_input }
        // console.log('values2:', values2)
        const res = await this.report_print({
          values: values2
          //   context: { report_type: 'month' }
        })
        console.log('res:', res)
      }
    }
    return ModelClass
  }
}

const AccountingReport = {
  _name: 'accounting.report',
  _inherit: 'account.report.base',

  metadata: {
    description: '',

    context: {
      default_debit_credit: true
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      // async default_get_onchange(payload = {}) {
      //   const values = await super.default_get({ ...payload })
      //   const values2 = { debit_credit: true }
      //   return { ...values, ...values2 }
      // }

      async report_print(payload = {}) {
        // const { values } = payload
        // const { date_from } = values
        // const payload2 = { ...payload, values: { ...values, date_from: null } }
        const res = await super.report_print(payload)
        // console.log('res 1:', res)
        const { data = {}, context = {}, get_account_lines = [] } = res
        const { date_from, date_to } = data
        const { report_type } = context

        const date_month = date_fn.date2str_month(new Date(date_from))

        const res2 = {
          date_from,
          date_month,
          date_to,
          report_type,
          lines: get_account_lines
        }
        return res2
      }
    }
    return ModelClass
  }
}

const AccountingReportBalancesheet = {
  _name: 'accounting.report',
  _inherit: 'accounting.report',
  metadata: {
    _isReport: 1,
    description: '',

    dataColumns: [
      { title: '名称', key: 'name2' },
      // { title: 'name', key: 'name' },
      // { title: 'acc type', key: 'account_type' },
      // { title: 'type', key: 'type' },
      // { title: 'lev', key: 'level' },
      { title: '期初借方', key: 'debit_open' },
      { title: '期初贷方', key: 'credit_open' },
      { title: '本期借方', key: 'debit' },
      { title: '本期贷方', key: 'credit' },
      { title: '期末借方', key: 'debit_close' },
      { title: '期末贷方', key: 'credit_close' }
      // { title: '借贷合计', key: 'balance' }
    ]
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async default_get_onchange(payload) {
        const { context: context_src = {} } = payload || {}
        const default_account_report_id = await this.ref(
          'accounting_pdf_reports.account_financial_report_balancesheet0'
        )
        const context = {
          ...context_src,
          default_account_report_id: default_account_report_id[1] // # 余额表
        }

        const values = await super.default_get_onchange({ ...payload, context })
        return { ...values }
      }

      async report_print(payload = {}) {
        const res = await super.report_print(payload)
        // console.log('res 1:', res)
        const { lines } = res
        const lines1 = lines.filter(item => item.type2 === 'assets')
        const lines2 = lines.filter(item => item.type2 === 'liability')

        const res2 = { ...res, lines: lines1, lines2 }

        return res2
      }
    }
    return ModelClass
  }
}

const AccountingReportProfitandloss = {
  _name: 'accounting.report',
  _inherit: 'accounting.report',

  metadata: {
    _isReport: 1,
    description: '',

    dataColumns: [
      { title: '名称', key: 'name2' },
      // { title: 'name', key: 'name' },
      // { title: 'acc type', key: 'account_type' },
      // { title: 'type', key: 'type' },
      // { title: 'lev', key: 'level' },
      { title: '借方', key: 'debit' },
      { title: '贷方', key: 'credit' },
      { title: '余额', key: 'balance' }
    ]
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async default_get_onchange(payload) {
        const { context: context_src = {} } = payload || {}
        const default_account_report_id = await this.ref(
          'accounting_pdf_reports.account_financial_report_profitandloss0' // 损益表
        )
        const context = {
          ...context_src,
          default_account_report_id: default_account_report_id[1]
        }

        const values = await super.default_get_onchange({ ...payload, context })
        return { ...values }
      }

      async report_print(payload = {}) {
        const res = await super.report_print(payload)
        // console.log('res 1:', res)
        const { lines: lines_src } = res

        const lines = lines_src.map(item => {
          const full_name = `${item.type},${item.account_type},${item.name}`

          let name2 = item.name
          if (full_name === 'report,sum,Profit and Loss') {
            name2 = '总计'
          } else if (full_name === 'report,account_type,Income') {
            name2 = '收入'
          } else if (full_name === 'report,account_type,Expense') {
            name2 = '成本'
          }

          return { ...item, name2 }
        })

        const res2 = { ...res, lines }

        return res2
      }
    }
    return ModelClass
  }
}

const AccountReportGeneralLedger = {
  _name: 'account.report.general.ledger',
  _inherit: 'account.report.base',

  metadata: {
    _isReport: 1,
    description: '',

    context: {
      default_initial_balance: true
    },

    dataColumns: [
      {
        type: 'expand',
        width: 30,
        render: (h, params) => {
          const { row } = params
          // console.log(row)
          const child_cols = [
            // { title: 'lref', key: 'lref' },
            // { title: 'lcode', key: 'lcode' },
            // { title: 'lid', key: 'lid' },
            { title: '日期', key: 'ldate' },
            { title: '摘要', key: 'lname' },
            { title: '凭证编号', key: 'move_name' },
            { title: '业务伙伴', key: 'partner_name' },
            { title: '借方', key: 'debit' },
            { title: '贷方', key: 'credit' },
            { title: '余额', key: 'balance' }
          ]
          return h(
            'Table',
            { props: { columns: child_cols, data: row.move_lines } },
            ''
          )
        }
      },
      { title: '科目', key: 'name2' },
      // { title: '科目编码', key: 'code' },
      // { title: '科目名称', key: 'name' },
      { title: '借方', key: 'debit' },
      { title: '贷方', key: 'credit' },
      { title: '余额', key: 'balance' }
    ]
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async report_print(payload = {}) {
        const res = await super.report_print(payload)
        // console.log('res 1:', res)
        const { data = {}, context = {}, Accounts = [] } = res
        const { date_from, date_to } = data
        const { report_type } = context

        const date_month = date_fn.date2str_month(new Date(date_from))

        const lines = Accounts.map(item => {
          const name2 = `${item.code} ${item.name}`

          return {
            ...item,
            name2,
            move_lines: item.move_lines.map(it => {
              return {
                ...it,
                lname: it.lid === 0 ? '起初余额' : it.lname
              }
            })
          }
        })

        const res2 = {
          date_from,
          date_to,
          date_month,
          report_type,
          lines
        }
        return res2
      }
    }
    return ModelClass
  }
}

const AccountReportPartnerLedger = {
  _name: 'account.report.partner.ledger',
  _inherit: 'account.report.base',

  metadata: {
    _isReport: 1,
    description: '',

    context: {
      default_result_selection: 'customer_supplier'
    },

    dataColumns: [
      {
        type: 'expand',
        width: 30,
        render: (h, params) => {
          const { row } = params
          console.log(row)
          const child_cols = [
            // { title: 'id', key: 'id' },
            { title: '日期', key: 'date' },
            // { title: 'code', key: 'code' },
            // { title: 'a-code', key: 'a-code' },
            // { title: 'a-name', key: 'a-name' },
            // { title: 'ref', key: 'ref' },
            { title: '凭证编号', key: 'move_name' },
            { title: '摘要', key: 'name' },
            // { title: 'displayed_name', key: 'displayed_name' },
            { title: '借方', key: 'debit' },
            { title: '贷方', key: 'credit' },
            { title: '余额', key: 'progress' }
          ]
          return h(
            'Table',
            { props: { columns: child_cols, data: row.lines } },
            ''
          )
        }
      },
      { title: '业务伙伴', key: 'partner_name' },
      { title: '借方', key: 'debit' },
      { title: '贷方', key: 'credit' },
      { title: '余额', key: 'balance' }
    ]
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async report_print(payload = {}) {
        const res = await super.report_print(payload)
        // console.log('res 1:', res)
        const { data = {}, context = {}, report_data = [] } = res
        const { date_from, date_to, result_selection } = data.form
        const { report_type } = context
        const date_month = date_fn.date2str_month(new Date(date_from))

        const lines = report_data.map(item => {
          return {
            ...item,
            lines: item.lines.map(it => {
              return {
                ...it,
                name: it.id === 0 ? '起初余额' : it.name,
                move_name: it.id === 0 ? '' : it.move_name
              }
            })
          }
        })

        const res2 = {
          date_from,
          date_to,
          date_month,
          result_selection,
          report_type,
          lines
        }
        return res2
      }
    }
    return ModelClass
  }
}

const AccountReportPartnerLedgerCustomer = {
  _name: 'account.report.partner.ledger',
  _inherit: 'account.report.partner.ledger',

  metadata: {
    _isReport: 0,
    description: '',
    context: {
      default_result_selection: 'customer'
    }
  }
}

const AccountReportPartnerLedgerSupplier = {
  _name: 'account.report.partner.ledger',
  _inherit: 'account.report.partner.ledger',

  metadata: {
    _isReport: 0,
    description: '',
    context: {
      default_result_selection: 'supplier'
    }
  }
}

const AccountBalanceReport = {
  _name: 'account.balance.report',
  _inherit: 'account.report.base',

  metadata: {
    _isReport: 1,
    description: '试算平衡',

    dataColumns: [
      { title: '科目编码', key: 'code' },
      { title: '科目名称', key: 'name' },
      { title: '借方', key: 'debit' },
      { title: '贷方', key: 'credit' },
      { title: '借贷合计', key: 'balance' }
    ]
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async report_print(payload = {}) {
        const res = await super.report_print(payload)
        console.log('res 1:', res)
        const { data = {}, context = {}, Accounts = [] } = res
        const { date_from, date_to } = data
        const { report_type } = context
        const date_month = date_fn.date2str_month(new Date(date_from))

        const res2 = {
          date_from,
          date_to,
          date_month,
          report_type,
          lines: Accounts
        }
        return res2
      }
    }
    return ModelClass
  }
}

const Models = {
  'account.report.base': AccountReportBase,

  'accounting.report': AccountingReport,
  'accounting.report.balancesheet': AccountingReportBalancesheet,
  'accounting.report.profitandloss': AccountingReportProfitandloss,

  'account.report.general.ledger': AccountReportGeneralLedger,
  'account.report.partner.ledger': AccountReportPartnerLedger,
  'account.report.partner.ledger.customer': AccountReportPartnerLedgerCustomer,
  'account.report.partner.ledger.supplier': AccountReportPartnerLedgerSupplier,

  'account.balance.report': AccountBalanceReport
}

export default Models
