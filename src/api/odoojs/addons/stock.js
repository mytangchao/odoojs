const StockPicking = {
  metadata: {
    description: '',
    fields: {},

    fieldsForQuery: {
      name: null,
      scheduled_date: null,
      partner_id: null,
      origin: null,
      state: null
    },
    columnsForPage: {
      name: { readonly: true, label: '编号' },
      origin: { readonly: true, label: '源单' },
      scheduled_date: { type: 'datetime', label: '日期' },
      partner_id: { type: 'select', label: '供应商' },
      state: { readonly: true, type: 'select', label: '状态' }
    },

    columnsForList: {
      name: null,
      scheduled_date: null,
      partner_id: null,
      origin: null,
      state: null
    }
  }
}

const Models = {
  'stock.picking': StockPicking
}

export default Models
