const PurchaseOrder = {
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',
    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: false, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '编辑' },
    btnDel: { hidden: rec => rec.state !== 'draft', label: '删除' },

    fields: {},

    field_onchange_ref: 'purchase.purchase_order_form',
    // field_onchange_model: 'purchase.order',
    // field_onchange_field: 'order_line',

    fieldsForQuery: {
      name: null,
      date_order: null,
      date_approve: null,
      date_planned: null,
      partner_id: null,
      user_id: null,
      amount_total: null,
      state: null,
      invoice_status: null
    },

    fieldsForBrowse: {
      state: null,
      name: null,
      partner_id: null,
      date_order: null,
      date_approve: null,
      date_planned: null,

      origin: null,
      company_id: null,
      invoice_ids: null,
      user_id: null,
      invoice_status: null,

      currency_id: null,
      amount_untaxed: null,
      amount_tax: null,
      amount_total: null,
      notes: null,

      order_line: null,

      picking_count: null,
      picking_type_id: null,
      invoice_count: null,
      default_location_dest_id_usage: null

      // picking_ids: null,
      // partner_ref: null,
      // is_shipped: null,

      // dest_address_id: null,
      // incoterm_id: null,
      // payment_term_id: null,
      // fiscal_position_id: null

      // message_follower_ids: null,
      // activity_ids: null,
      // message_ids: null,
      // message_attachment_count: null
    },

    context: {
      quotation_only: true
    },

    columnsForFilter: {
      partner_id: { type: 'select', label: '供应商', placeholder: '供应商' }
    },

    workflow: {
      // 控制 steps 显示用的
      state: rec => {
        // console.log('fn, state,', rec.state)
        // draft, sent,
        // purchase, to invoice, invoiced
        // done, cancel,

        if (rec.state !== 'purchase') {
          return rec.state
        } else {
          if (rec.invoice_status === 'invoiced') {
            return rec.invoice_status
          } else {
            return rec.state
          }
        }
      },
      steps: {
        draft: { string: '草稿' }, // 总是显示,
        sent: { string: '已发送', hidden: rec => rec.state !== 'send' },
        purchase: {
          string: rec =>
            ['draft', 'sent'].includes(rec.state)
              ? '待确认'
              : ['purchase'].includes(rec.state)
              ? '已确认'
              : '确认'
        },
        invoiced: {
          string: rec => {
            if (['draft', 'sent'].includes(rec.state)) {
              return '登记账单'
            }

            if (rec.state === 'purchase') {
              const invoice_status_map = {
                'to invoice': '登记账单',
                invoiced: '已登记账单',
                no: '账单'
              }
              return invoice_status_map[rec.invoice_status] || '账单'
            }

            return '账单'
          },

          hidden: rec => rec.state === 'cancel'
        },
        done: { string: '锁定', hidden: rec => rec.state !== 'done' }, // 仅在 锁定后显示
        cancel: { string: '已取消', hidden: rec => rec.state !== 'cancel' } // 仅在 取消后显示
      },

      buttons: {
        button_confirm: {
          type: 'object',
          string: '确认',
          message: '确认订单?',
          btn_type: 'primary',
          hidden: rec => !['draft', 'sent'].includes(rec.state)
        },

        create_invoices: {
          type: 'object',
          string: '登记账单',
          message: '确认登记账单?',
          btn_type: 'primary',
          hidden: rec => rec.invoice_status !== 'to invoice'
        },

        // action_view_invoice: {
        //   type: 'object_action',
        //   string: '登记账单',
        //   message: '确认登记账单?',
        //   btn_type: 'primary',
        //   hidden: rec => rec.invoice_status !== 'to invoice',
        //   context: { create_bill: true, quotation_only: true },
        //   path: '/form/AccountMoveInvoiceIn'
        // },

        search_invoice: {
          type: 'router',
          string: '查看账单',
          hidden: rec => rec.invoice_status !== 'invoiced',
          path: '/list/AccountMoveInvoiceIn',
          domain(rec) {
            return {
              id: rec.invoice_ids
            }
          }
        },

        button_cancel: {
          type: 'object',
          string: '取消',
          message: '取消订单?',
          hidden: rec =>
            !['draft', 'to approve', 'sent', 'purchase'].includes(rec.state)
        },
        button_draft: {
          type: 'object',
          string: '重置为草稿',
          message: '重置为草稿?',
          hidden: rec => rec.state !== 'cancel'
        }
      }
    },

    cardForView: {},

    columnsForPage: {
      name: { readonly: true, label: '订单号' },
      // state: { readonly: true, type: 'select', label: '状态' },
      // invoice_status: { readonly: true, type: 'select', label: '账单状态' },

      partner_id: { type: 'select', label: '供应商' },
      partner_ref: { readonly: true, label: '供应商单号' },
      // date_order: { type: 'datetime', label: '日期' },
      date_order: { type: 'date', label: '单据日期' },
      date_approve: { type: 'date', label: '确认日期' },
      date_planned: { type: 'date', label: '计划日期' },
      user_id: { type: 'select', label: '采购员' },
      amount_total: { readonly: true, type: 'monetary', label: '合计' },

      state: { readonly: true, type: 'select', label: '状态' },
      invoice_status: { readonly: true, type: 'select', label: '票据状态' },

      notes: { type: 'textarea', label: '备注' },

      order_line: {
        type: 'table',
        label: '明细',
        odooModel: 'purchase.order.line', //
        vuxModel: 'purchaseOrderLine', // viewO2m, formO2m 需要这个去找模型
        refName: 'order_id' // // viewO2m, formO2m 需要这个补充 onchange 的 values
      }
    },

    columnsForList: {
      name: null,
      // date_order: null,
      date_approve: null,
      date_planned: null,
      partner_id: null,
      user_id: null,
      amount_total: null,
      state: null,
      invoice_status: null
    },

    columnsForView: {
      // name: null,

      partner_id: null,
      partner_ref: null,
      date_order: null,
      date_planned: null,
      user_id: null,
      notes: null,

      amount_total: null,
      invoice_status: null,
      order_line: null

      // state: null,
    },

    columnsForForm: {
      // name: null,
      // state: null,
      // invoice_status: null,

      partner_id: null,
      partner_ref: null,
      date_order: null,
      date_planned: null,
      user_id: null,
      notes: null,

      // amount_total: null,
      order_line: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      async action_view_invoice(rid, payload = {}) {
        return this.call('action_view_invoice', [rid], { ...payload })
      }

      async create_invoices(rid) {
        // 从 PO 到 Account Invoice.
        // 打开 AI 的 新增form,
        // 这里 直接 create 的 理由是什么呢? 忘记了. 可能是 fields_get 搞不定么?
        const context = { quotation_only: true, create_bill: true }
        const action_res = await this.action_view_invoice(rid, { context })
        const { context: context1 } = action_res
        const context2 = {
          ...context,
          ...context1,
          active_id: rid,
          active_ids: [rid],
          active_model: 'purchase.order'
        }
        const inv_obj = this.env('account.move.invoice.in')

        const res1 = await inv_obj.default_get_onchange({ context: context2 })
        const { value_form } = res1
        const res = inv_obj.call('create', [value_form], { context: context2 })
        // console.log('create_invoices, res', res)
        return res
      }
    }
    return ModelClass
  }
}

const PurchaseOrderLine = {
  metadata: {
    description: '',

    fields: {},
    field_onchange_ref: 'purchase.purchase_order_form',
    field_onchange_model: 'purchase.order',
    field_onchange_field: 'order_line',

    //
    fieldsForQuery: {
      state: null,
      company_id: null,
      name: null,
      product_id: null,
      product_uom: {
        // SO PO MO 三个模型, 在edit 时, 子模型中有 product_uom 字段 需要额外处理
        forO2mEditFieldsGet: 1
      },
      product_uom_category_id: null,
      price_unit: null,

      product_qty: null,

      qty_received_method: null,
      qty_received: null,
      qty_invoiced: null,
      date_planned: null,
      propagate_cancel: null,

      taxes_id: null,
      display_type: null,
      account_analytic_id: null,
      analytic_tag_ids: null,
      invoice_lines: null,
      move_ids: null
    },

    // listPage 表格控制
    tableForList: {
      border: true
    },

    columnsForPage: {
      company_id: { type: 'select', label: '公司' },
      state: { type: 'select', label: '状态' },
      product_id: { type: 'select', label: '产品' /* tooltip: true */ },
      name: { label: '摘要' },
      product_qty: { type: 'number', align: 'right', label: '数量' },
      qty_received: { type: 'number', align: 'right', label: '已收货' },
      qty_invoiced: { readonly: true, align: 'right', label: '已登记账单' },
      price_unit: { type: 'monetary', label: '单价', align: 'right' },
      // price_total: { type: 'monetary', label: '合计', align: 'right' },
      price_subtotal: { type: 'monetary', align: 'right', label: '小记' }
    },

    columnsForList: {
      product_id: null,
      name: null,
      product_qty: { align: 'right' },
      qty_received: {},
      qty_invoiced: null,
      price_unit: { align: 'right' },
      // price_total: {  align: 'right' },
      price_subtotal: { align: 'right' }
      // state: {}
    },
    columnsForForm: {
      product_id: null,
      name: null,
      product_qty: null,
      price_unit: null,
      price_subtotal: null
      // price_total: null,
      // qty_received: null,
      // qty_invoiced: null,
      // state: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      get_globals_dict({ values = {}, globals_dict = {} }) {
        return super.get_globals_dict({
          values,
          globals_dict: {
            ...globals_dict,
            product_uom_category_id: values.product_uom_category_id
          }
        })
      }
    }

    return ModelClass
  }
}

const Models = {
  'purchase.order': PurchaseOrder,
  'purchase.order.line': PurchaseOrderLine
}

export default Models
