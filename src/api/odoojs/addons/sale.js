const SaleOrder = {
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',

    // 动态创建 创建 / 删除 vuex module
    relation_models: ['sale.order.line'],

    fields: {},
    field_onchange_ref: 'sale.view_order_form',
    // field_onchange_model: 'sale.order',
    // field_onchange_field: 'order_line',

    btnNew: { hidden: false, label: '新增' },
    btnView: { hidden: false, label: '查看' },
    btnEdit: { hidden: rec => rec.state !== 'draft', label: '编辑' },
    btnDel: { hidden: rec => rec.state !== 'draft', label: '删除' },

    fieldsForQuery: {
      state: null,
      name: null,
      partner_id: null,
      company_id: null,
      user_id: null,
      reference: null,
      note: null,

      date_order: null,

      //   create_date: null,
      commitment_date: null,
      expected_date: null,
      amount_total: null,

      invoice_status: null

      // order_line: null

      // validity_date: None,
      // payment_term_id: None,
      // amount_untaxed: None,
      // amount_tax: None,
      // amount_total: None,

      // team_id: None,
      // client_order_ref: None,
      // fiscal_position_id: None,
      // analytic_account_id: None,
      // invoice_status: None,
      // commitment_date: None
    },

    fieldsForSearch: undefined,

    fieldsForBrowse: {
      state: null,

      display_name: null,
      name: null,
      partner_id: null,
      validity_date: null,
      date_order: null,
      user_id: null,
      invoice_ids: null,
      note: null,

      // 这里数据不够也影响 onchange 返回值
      currency_id: null,
      pricelist_id: null,

      amount_untaxed: null,
      amount_tax: null,
      amount_total: null,

      order_line: {},
      client_order_ref: null,
      origin: null

      // currency_id: null,
      // partner_invoice_id: null,
      // partner_shipping_id: null,
      // payment_term_id: null,
      // pricelist_id: null

      // delivery_count: None,
      // invoice_count: None,

      // authorized_transaction_ids: None,
      // picking_ids: None,

      // sale_order_template_id: None,

      // sale_order_option_ids: None,
      // require_signature: None,
      // require_payment: None,
      // warehouse_id: None,
      // incoterm: None,
      // picking_policy: None,
      // expected_date: None,
      // effective_date: None,
      // origin: None,
      // campaign_id: None,
      // medium_id: None,
      // source_id: None,
      // signed_by: None,
      // signed_on: None,
      // signature: None
    },

    columnsForFilter: {
      partner_id: {
        type: 'select',
        domain: [],
        label: '客户',
        placeholder: '客户'
      }
    },

    forAppList: {
      title: rec => `${rec.name}`,
      // icon: rec => {
      //   // 编辑的 新增的时候 没有 product_id__object 字段
      //   return `${(rec.product_id__object || {}).image_128__url}`
      // },

      label: rec => `${rec.date_order || ''}`,
      value: rec => `${rec.state__name} ${rec.invoice_status__name}`
    },

    // 工作流控制
    workflow: {
      // 控制 steps 显示用的
      state: rec => {
        // console.log('fn, state,', rec.state)
        // draft, sent,
        // sale, to invoice, invoiced
        // done, cancel,

        if (rec.state !== 'sale') {
          return rec.state
        } else {
          if (rec.invoice_status === 'invoiced') {
            return rec.invoice_status
          } else {
            return rec.state
          }
        }
      },

      // 工作流的步骤
      steps: {
        draft: { string: '草稿' },
        sent: { string: '已发送', hidden: rec => rec.state !== 'send' },
        sale: {
          string: rec =>
            ['draft', 'sent'].includes(rec.state)
              ? '待确认'
              : ['sale'].includes(rec.state)
              ? '已确认'
              : '确认'
        },
        invoiced: {
          string: rec =>
            ['draft', 'sent'].includes(rec.state) ||
            (rec.state === 'sale' && rec.invoice_status === 'to invoice')
              ? '待开票'
              : '已开票',
          hidden: rec => rec.state === 'cancel'
        },
        done: { string: '锁定', hidden: rec => rec.state !== 'done' }, // 仅在 锁定后显示
        cancel: { string: '取消', hidden: rec => rec.state !== 'cancel' } // 仅在 取消后显示
      },

      buttons: {
        action_confirm: {
          type: 'object',
          string: '确认',
          message: '确认订单?',
          btn_type: 'primary',
          hidden: rec => !['draft', 'sent'].includes(rec.state)
        },
        create_invoice: {
          type: 'action',
          string: '开票',
          message: '确认要开票?',
          btn_type: 'primary',
          hidden: rec => rec.invoice_status !== 'to invoice',

          wizard: {
            submit: true,
            model: 'sale.advance.payment.inv',

            vals: {},
            context: {},
            fields: {},
            method: 'create_invoices'
          }
        },
        search_invoice: {
          type: 'router',
          string: '查看销售账单',
          message: '查看销售账单?',
          hidden: rec => rec.invoice_status !== 'invoiced',
          path: '/list/AccountMoveInvoiceOut',
          domain(rec) {
            return {
              id: rec.invoice_ids
            }
          }
        },

        action_cancel: {
          type: 'object',
          string: '取消',
          message: '取消订单?',
          hidden: rec => !['draft', 'sent', 'sale'].includes(rec.state)
        },
        action_draft: {
          type: 'object',
          string: '重置为草稿',
          message: '重置为草稿?',
          hidden: rec => rec.state !== 'cancel'
        }
      }
    },

    // viewPage 标题 控制
    cardForView: {},

    // page 各个字段的统一定义, 包括 list view 和 form
    columnsForPage: {
      name: { readonly: true, label: '订单号' },
      partner_id: { type: 'select', label: '客户' },
      validity_date: { type: 'date', label: '到期日期' },
      date_order: { type: 'date', label: '订购日期' },
      commitment_date: { label: '交货日期' },
      expected_date: { label: '预计日期' },
      user_id: { type: 'select', label: '销售员' },
      amount_total: { readonly: true, type: 'monetary', label: '合计' },

      state: { readonly: true, type: 'select', label: '状态' },
      invoice_status: { readonly: true, type: 'select', label: '票据状态' },
      note: { type: 'textarea', label: '备注' },

      //   create_date: { type: 'date', label: '创建日期' },

      order_line: {
        type: 'table',
        label: '明细',
        odooModel: 'sale.order.line', // viewO2m, formO2m 需要这个去找模型
        vuxModel: 'saleOrderLine', // viewO2m, formO2m 需要这个去找模型
        refName: 'order_id' // // viewO2m, formO2m 需要这个补充 onchange 的 values
      }
    },

    columnsForList: {
      name: null,
      date_order: null,
      commitment_date: null,
      expected_date: null,
      partner_id: null,
      user_id: null,
      amount_total: null,
      state: null,
      invoice_status: null
    },

    columnsForView: {
      // name: null,
      partner_id: null,
      validity_date: null,
      date_order: null,
      user_id: null,
      amount_total: null,
      note: null,
      invoice_status: null,
      order_line: null

      // state: null,
    },

    columnsForForm: {
      // name: null,
      partner_id: null,
      // validity_date: null,
      date_order: null,
      // user_id: null,
      note: null,
      amount_total: null,
      order_line: null
      // state: null,
      // invoice_status: null,
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {}

    return ModelClass
  }
}

const SaleAdvancePaymentInv = {
  metadata: {}
}

const SaleOrderLine = {
  metadata: {
    description: '',

    fields: {
      product_id: { type: 'many2one', relation: 'product.product' }
      // image_128: { type: 'image' },
    },

    field_onchange_ref: 'sale.view_order_form',
    field_onchange_model: 'sale.order',
    field_onchange_field: 'order_line',

    context: {},

    fieldsForQuery: {
      // order_id: {
      //   name: null,
      //   date_order: null
      // },
      // order_partner_id: null,
      product_id: null,
      name: null,
      product_uom_qty: null,
      product_uom: null,
      product_uom_category_id: null,
      price_unit: null,
      price_total: null,
      price_subtotal: null,
      company_id: null

      // invoice_status: null,
      // state: null
    },

    fieldsForSearch: undefined,

    fieldsForBrowse: {
      name: null,
      product_id: null,
      product_uom_qty: null,
      product_uom: {
        // SO PO MO 三个模型, 在edit 时, 子模型中有 product_uom 字段 需要额外处理
        forO2mEditFieldsGet: 1
      },
      product_uom_category_id: null,

      price_unit: null,
      price_subtotal: null,
      price_total: null,

      state: null,
      invoice_status: null,

      qty_to_deliver: null,
      qty_delivered: null,
      qty_delivered_manual: null,
      qty_delivered_method: null,
      company_id: null

      // scheduled_date: None,
      // qty_invoiced: None,
      // qty_to_invoice: None,
      // discount: None,

      // sequence: None,
      // display_type: None,
      // product_updatable: None,
      // product_template_id: None,
      // analytic_tag_ids: None,
      // route_id: None,
      // product_type: None,
      // virtual_available_at_date: None,
      // qty_available_today: None,
      // free_qty_today: None,
      // warehouse_id: None,
      // is_mto: None,
      // display_qty_widget: None,
      // customer_lead: None,
      // product_packaging: None,
      // tax_id: None,
      // currency_id: None,
      // price_tax: None
    },

    // listPage 表格控制
    tableForList: {
      border: true
    },

    // 移动端用, 暂时 保留
    forAppList: {
      title: rec => {
        return `${rec.name}`
      },
      icon: rec => {
        return `${(rec.product_id__object || {}).image_128__url || ''}`
      },

      // label: rec => {
      //   return `${rec.email || ''}`
      // },
      value: rec => {
        return `¥${rec.price_subtotal}=${rec.product_uom_qty} * ¥${rec.price_unit}`
      }
    },

    columnsForPage: {
      // 这里的 type 起到了 fields_get 未获取之前, 处理显示用
      company_id: { type: 'select', label: '公司' },
      state: { type: 'select', label: '状态' },
      invoice_status: { type: 'select', label: '开票状态' },
      product_id: { type: 'select', label: '产品', tooltip: true },
      product_uom: { type: 'select', label: '单位' },
      product_uom_category_id: { type: 'select', label: '单位类别' },
      name: { label: '说明' /*tooltip: true */ },

      product_uom_qty: { type: 'number', align: 'right', label: '数量' },

      qty_delivered: { type: 'number', align: 'right', label: '已送货' },
      qty_invoiced: { readonly: true, align: 'right', label: '已开票' },
      price_unit: { type: 'monetary', label: '单价', align: 'right' },
      // price_total: { type: 'monetary', label: '合计', align: 'right' },
      price_subtotal: { type: 'monetary', align: 'right', label: '小记' }

      // order: { label: '订单号, 日期, 客户' },
      // // order_id: { type: 'select', label: '订单号', width: 90 },
      // // date_order: { type: 'date', label: '日期', width: 120 },
      // // order_partner_id: { type: 'select', label: '客户', width: 80 },
      // id: { label: 'ID' },
    },

    columnsForList: {
      product_id: null,
      name: null,
      product_uom_qty: null,
      qty_delivered: {},
      qty_invoiced: null,
      price_unit: null,
      price_subtotal: null
    },

    columnsForForm: {
      product_id: null,
      name: null,
      product_uom_qty: null,
      price_unit: null,
      price_subtotal: null

      // product_uom: null,
      // product_uom_category_id: null

      // qty_delivered: null
      // qty_invoiced: null,
      // price_total: null,
      // invoice_status: null,
      // state: null
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      get_globals_dict({ values = {}, globals_dict = {} }) {
        return super.get_globals_dict({
          values,
          globals_dict: {
            ...globals_dict,
            product_uom_category_id: values.product_uom_category_id
          }
        })
      }
    }

    return ModelClass
  }
}

const Models = {
  'sale.order': SaleOrder,
  'sale.order.line': SaleOrderLine,
  'sale.advance.payment.inv': SaleAdvancePaymentInv
}

export default Models
