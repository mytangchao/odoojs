const ProductTemplate = {
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',

    // 这里定义基本的字段属性 可被其他地方利用
    // 目前实现了:
    // 1 图片字段的处理, saerch/browse 后, 生成 图片字段的 url
    fields: {
      image_128: { type: 'image' }
    },

    field_onchange_ref: 'product.product_template_only_form_view',
    // field_onchange_model: 'product.template',
    // field_onchange_field: '',

    //
    fieldsForQuery: {
      display_name: null,
      name: null,
      type: null,
      default_code: null,
      lst_price: null,
      standard_price: null
    },

    context: {
      // 默认 采购开票方式 为 订购数量
      // 这样 可以 无 库存模块 完成 财务工作
      // 应该设置一个 页面 控制该变量
      // 核心设计思想是, 销售采购财务各模块独立,允许手工做账
      default_purchase_method: 'purchase',

      // 创建产品时 设置默认公司, 这样 确保 自己管理自己的产品
      default_company_id: self => {
        const company_id = self.get_company_id()
        return company_id
      }
    },

    columnsForPage: {
      id: { label: 'ID' },
      image_128: { type: 'img', label: '图片' },
      default_code: { label: '编码' },
      name: { label: '名称' },
      type: { type: 'select', label: '产品类型' },
      lst_price: { type: 'monetary', label: '销售价格' },
      standard_price: { type: 'monetary', label: '成本价格' },
      image_1920: { type: 'img', url: 'image_128__url', label: '图片' }
    },

    columnsForList: {
      // id: { label: 'ID' },
      image_128: null,
      default_code: null,
      name: null,
      type: null,
      lst_price: null,
      standard_price: null
    },

    columnsForView: {
      // id: null,
      default_code: null,
      name: null,
      type: null,
      lst_price: null,
      standard_price: null,
      image_128: null
    },
    columnsForForm: {
      // id: null,
      default_code: null,
      name: null,
      type: null,
      lst_price: null,
      standard_price: null,
      image_1920: null
    }
  }
}

const ProductProduct = {
  metadata: {
    _isMenu: 1, // 控制菜单用的,
    description: '',

    // 这里定义基本的字段属性 可被其他地方利用
    // 目前实现了:
    // 1 图片字段的处理, saerch/browse 后, 生成 图片字段的 url
    fields: {
      image_128: { type: 'image' },
      image_1920: { type: 'image' }
    },

    //
    fieldsForQuery: {
      name: null,
      default_code: null,
      lst_price: null,
      standard_price: null
      // product_template_attribute_value_ids: null
    },

    columnsForPage: {
      id: { label: 'ID' },
      image_128: { type: 'img', label: '图片' },
      default_code: { label: '编码' },
      name: { label: '名称' },
      lst_price: { type: 'monetary', label: '销售价格' },
      standard_price: { type: 'monetary', label: '成本价格' }

      // product_template_attribute_value_ids__names: { label: '属性值' }
    },

    columnsForList: {
      id: null,
      image_128: null,
      default_code: null,
      name: null,
      // product_template_attribute_value_ids__names: null,
      lst_price: null,
      standard_price: null
    }
  }
}

const ProductProductExpense = {
  _inherit: 'product.product',
  metadata: {
    description: '',

    fields: {},

    fieldsForQuery: {},

    columnsForList: {
      image_128__url: { type: 'img', label: '图片' },
      default_code: { label: '编码' },
      name: { label: '名称' },
      product_template_attribute_value_ids__names: { label: '属性值' },
      lst_price: { type: 'monetary', label: '销售价格' },
      standard_price: { type: 'monetary', label: '成本价格' }
    }
  },

  extend: BaseClass => {
    class ModelClass extends BaseClass {
      global_domain(payload = {}) {
        // const { domain = {}, domain2 = [] } = payload
        // return super.global_domain(payload)
        const { domain = {} } = payload
        return super.global_domain({
          ...payload,
          domain: { ...domain, can_be_expensed: true }
        })
      }

      async create(values = {}, kwargs = {}) {
        const values2 = { ...values, can_be_expensed: true }
        return super.create(values2, kwargs)
      }
    }
    return ModelClass
  }
}

const Models = {
  'product.template': ProductTemplate,
  'product.product': ProductProduct,
  'product.product.expense': ProductProductExpense
}

export default Models
