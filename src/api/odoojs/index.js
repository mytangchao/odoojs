import { ODOO as OdooBase } from './odoo'

import vuex_store_creator from './vuex_store'

export class ODOO extends OdooBase {
  //
  constructor(params) {
    super(params)
    this.vuex_store = vuex_store_creator(this)
  }
}

// odoojs 对外的出口
const odooCreator = payload => {
  return new ODOO(payload)
}

export default odooCreator
