export const get_image = (model, res_id, field) => {
  const baseURL = process.env.VUE_APP_BASE_API
  const imgUrl = '/web/image'
  if (!res_id) {
    return ''
  }
  return `${baseURL}${imgUrl}?model=${model}&id=${res_id}&field=${field}`
}

export const model_odoo2vuex = name =>
  name
    .replace(/(\.|^)[a-z]/g, L => L.toUpperCase())
    .replace(/\./g, '')
    .replace(/( |^)[A-Z]/g, L => L.toLowerCase())

export const model_vuex2odoo = name =>
  name.replace(/[A-Z]/g, L => `.${L.toLowerCase()}`)
