// 简单的 domain, 都是 与 关系的, 可以用 dict 传参数,
// 复杂的 domain, 有 或 非 关系的, 依然可以用 list 格式

// global_domain 是个函数, 用于设置全局的/与用户有关的过滤条件
async function _format_domain(self, domain_src = {}) {
  // console.log('_format_domain ,', self.model, self, domain_src)

  const { domain: global, domain2: global2 } = await self.global_domain()
  const { domain: domain_default = {} } = self.metadata || {}

  let domain = []
  let domain_dict = {}
  if (Array.isArray(domain_default)) {
    domain = [...domain, ...domain_default]
  } else {
    domain_dict = { ...domain_dict, ...domain_default }
  }

  if (Array.isArray(domain_src)) {
    domain = [...domain, ...domain_src]
  } else {
    domain_dict = { ...domain_dict, ...domain_src }
  }

  if (Array.isArray(global)) {
    domain = [...domain, ...global]
  } else {
    domain_dict = { ...domain_dict, ...global }
  }

  if (Array.isArray(global2)) {
    domain = [...domain, ...global2]
  } else {
    domain_dict = { ...domain_dict, ...global2 }
  }

  return [domain, domain_dict]
}

class ModelClassMeta {
  constructor(options = {}) {
    // 通过 metadata 携带一些 model 的参数
    const { model, metadata, odoo, rpc /*, env */ } = options
    this.model = model
    this.metadata = metadata || {}
    this.odoo = odoo
    this.rpc = rpc
  }

  env(model) {
    return this.odoo.env(model)
  }
  // Not Used
  ref(xmlid) {
    // get model and id from xmlid
    return this.env('ir.model.data').call('xmlid_to_res_model_res_id', [
      xmlid,
      true
    ])
  }

  get_userinfo() {
    return this.rpc.get_userinfo()
  }

  call(method, args, kwargs = {}) {
    // console.log('model, call, ', this.model, method, args, kwargs)
    const context = this.get_context(kwargs)
    const kwargs2 = { ...kwargs, context }

    return this.rpc.call(this.model, method, args, kwargs2)
  }

  get_company_id() {
    const userinfo = this.get_userinfo()
    const { company_id } = userinfo
    return company_id
  }

  get_context(kwargs) {
    const userinfo = this.get_userinfo()
    const { user_context = {}, company_id } = userinfo
    const allowed_company_ids = [company_id]

    const { context: contextForDefault = {} } = this.metadata || {}

    const meta_context = Object.keys(contextForDefault).reduce((acc, cur) => {
      const val = contextForDefault[cur]
      const val2 = typeof val === 'function' ? val(this) : val
      acc[cur] = val2
      return acc
    }, {})

    const { context: contextSrc = {} } = kwargs
    const context = {
      ...user_context,
      allowed_company_ids,
      ...meta_context,
      ...contextSrc
    }

    return context
  }
}

class ModelClassBase extends ModelClassMeta {
  // TBD  判断 数组或 字典
  async global_domain(payload = {}) {
    // to be overrided
    const { domain = {}, domain2 = [] } = payload
    return { domain, domain2 }
  }

  // search, browse, create, write 四个方法, 返回 records, 这个函数做额外处理
  get_record(res) {
    // console.log('model, get_record, ', this.model)
    // // 处理 m2o 字段
    // const m2o_res = this.get_record_m2o(res, fields)
    // 处理图片, m2o, o2m 图片
    const img_res = this.get_record_image(res)
    // 处理计算列
    const related_res = this.get_record_compute(res)
    return { ...img_res, ...related_res }
  }

  // get_record_m2o(res, fields) {
  //   // // console.log('model, get_record, ', this.model, this.metadata)
  //   // const { columnsForPage: cols = {} } = this.metadata
  //   // const res2 = Object.keys(res).reduce((acc, cur) => {
  //   //   const meta2 = cols[cur] || {}
  //   //   const meta = fields[cur] || {}
  //   //   const val = res[cur]
  //   //   if (['selection', 'many2one'].includes(meta.type)) {
  //   //     // console.log('model, get_record, ', this.model, cur, res[cur], meta)
  //   //     const [k, v] = val || [null, null]
  //   //     acc[cur] = k
  //   //     acc[`${cur}__name`] = v
  //   //   } else if (['one2many', 'many2many'].includes(meta.type)) {
  //   //     if (meta2.multiple) {
  //   //       acc[cur] = val
  //   //       // TBD: 2020-11-12, m2m 字段 做 selelct 时 , 什么时候做计算 搞定 name
  //   //       acc[`${cur}__name`] = val.map(item => item.toString()).join(',')
  //   //     }
  //   //   }
  //   //   return acc
  //   // }, {})
  //   // return res2
  // }

  // metadata.fields 中的 related, compute 参数 定义了 计算列
  get_record_compute(res) {
    const fields1 = this.metadata.fields || {}
    const fields_related = Object.keys(fields1).filter(
      item => fields1[item].related || fields1[item].compute
    )

    const get_related = (result, related) => {
      let val = result
      const flds = related.split('.')
      flds.forEach(item => {
        val = val[item]
      })
      return val
    }

    const ret = fields_related.reduce((acc, cur) => {
      // console.log('related, cur,', cur, fields1[cur])
      const related = fields1[cur].related
      const compute = fields1[cur].compute
      if (related) {
        acc[cur] = get_related(res, related)
      } else if (compute) {
        acc[cur] = fields1[cur].compute(res)
      }
      return acc
    }, {})

    return ret
  }

  // metadata.fields 中的 type === image, 则补上 图片链接. m2o, o2m, 字段的图片做递归处理
  get_record_image(res) {
    // console.log('get_record_image', this.model)
    // console.log('get_record_image', this)

    const userinfo = this.rpc.get_userinfo() || {}
    // console.log('get_record_image', userinfo)
    const session_id = userinfo.session_id

    const date = new Date().getTime()
    // const image_url2 = `${partner.image_128}&unique=${date}`
    const baseURL = process.env.VUE_APP_BASE_API
    const imgUrl = '/web/image'

    const fields1 = this.metadata.fields || {}

    const fields = Object.keys(fields1).filter(item =>
      ['image', 'many2one', 'many2many', 'one2many'].includes(
        fields1[item].type
      )
    )

    const image_urls = fields.reduce(
      (acc, cur) => {
        const meta = fields1[cur]
        if (meta.type === 'image') {
          acc[
            `${cur}__url`
          ] = `${baseURL}${imgUrl}?model=${this.model}&id=${res.id}&field=${cur}&unique=${date}&session_id=${session_id}`
          // `${baseURL}${imgUrl}?model=${this.model}&id=${res.id}&field=${cur}&unique=${date}`
        } else if (meta.type === 'many2one') {
          if (res[cur]) {
            // console.log('res[cur]', this.model, cur, res[cur])
            // console.log('res[cur]', meta.relation, cur, res[cur])

            const ref_fld = `${cur}__object`
            const ref_rec = {
              id: res[cur],
              name: res[`${cur}__name`]
            }

            const ref_images = this.env(meta.relation).get_record_image(ref_rec)
            acc[ref_fld] = { ...ref_rec, ...ref_images }
          }
        } else {
          // m2m or o2m

          if (res[cur] && res[cur].length) {
            // console.log('res[cur]', this.model, cur, res[cur])
            const ref_fld = `${cur}__objects`
            // console.log('res[cur]', this.model, cur, res[ref_fld])
            acc[ref_fld] = res[ref_fld].map(item => {
              const ref_images = this.env(meta.relation).get_record_image(item)

              return { ...item, ...ref_images }
            })
          }
        }

        return acc
      },
      { ...res }
    )

    return image_urls
  }
}

class ModelClassCrud extends ModelClassBase {
  // 参数 {domain, domain2}
  async search_count(query = {}) {
    // console.log('xxxxx, in model search_count', this)
    // console.log('xxxxx, in model search_count', this.model, query)

    // 为了 支持 两种格式的 domain, 这里的参数规范 做了 修改
    // 丢弃的 方法 是 直接 传 domain,
    // 现在采用的 方法 是 将 domain 和  domain2 放在 对象里
    // 注意 检查代码 中 使用到 search_count 的地方

    const {
      domain = {}, // 目前的前端 都是简单的 domian ={}
      domain2 = []
    } = query

    const method = 'search_count2'
    const [domain_list, domain_dict] = await _format_domain(this, domain)
    const args = []
    const kwargs = {
      domain: [...domain2, ...domain_list],
      domain2: domain_dict
    }
    return await this.call(method, args, kwargs)
  }

  async name_search(query = {}) {
    const { domain, domain2 = [] } = query
    const method = 'name_search2'
    const [domain_list, domain_dict] = await _format_domain(this, domain)
    const args = []
    const kwargs = {
      domain: [...domain2, ...domain_list],
      domain2: domain_dict
    }
    return await this.call(method, args, kwargs)
  }

  // 参数 { domain, domain2, fields, offset, limit, page, order}
  async web_search_read(query = {}) {
    const {
      domain = {}, // 目前的前端 都是简单的 domian ={}
      domain2 = [], // 支持 两种格式的 domain
      fields,
      page = 1,
      limit = 0,
      offset, // offset 与 page 二选一
      order
    } = query

    const [domain_list, domain_dict] = await _format_domain(this, domain)

    // console.log('ws,', this)
    const { fieldsForSearch = {}, orderForSearch } = this.metadata || {}
    const fields2 = Object.keys(
      fields || { ...fieldsForSearch, display_name: null }
    )
    const order2 = order || orderForSearch

    const context = this.get_context(query)

    const kwargs = {
      domain: [...domain2, ...domain_list],
      domain2: domain_dict,
      fields: fields2,
      offset: offset !== undefined ? offset : (page - 1) * limit,
      limit,
      sort: order2,
      context
    }

    // console.log('xxx, ws,', kwargs)
    const result = await this.rpc.callWebSearchRead(this.model, kwargs)

    const records = result.records
    const records2 = records.map(item => {
      const image_fields = this.get_record(item)
      const item2 = { ...item, ...image_fields }
      return item2
    })

    return { ...result, records: records2 }
  }

  async search(kwargs = {}) {
    const res = await this.web_search_read(kwargs)
    return res.records
  }

  // TBD 2020-10-25 干什么用的 不再用了
  // search_read(kwargs) {
  //   return this.call('search_read', [], kwargs)
  // }

  // 参数 { domain, domain2, fields, offset, limit, page, order}
  async search____not___used(query = {}) {
    // const sss = await this.fields_get()
    // console.log('xxxxxxx,fields_get', sss)
    const {
      domain = {}, // 目前的前端 都是简单的 domian ={}
      domain2 = [], // 支持 两种格式的 domain
      fields = {},
      page = 1,
      limit = 0,
      offset, // offset 与 page 二选一
      order
    } = query

    // console.log('xxxxxxx,search', domain2)

    const [domain_list, domain_dict] = await _format_domain(this, domain)

    const method = 'search_read2'
    const args = []
    const kwargs = {
      domain: [...domain2, ...domain_list],
      domain2: domain_dict,
      fields: fields || { name: null },
      offset: offset !== undefined ? offset : (page - 1) * limit,
      limit,
      order
    }

    const records = await this.call(method, args, kwargs)
    const records2 = records.map(item => {
      const image_fields = this.get_record(item)
      const item2 = { ...item, ...image_fields }
      return item2
    })

    return records2
  }

  // 参数 { domain, domain2, fields }
  async search_one(kwargs = {}) {
    const {
      fields,
      callback
      // context
    } = kwargs
    const { fieldsForBrowse = {} } = this.metadata || {}
    const fields2 = fields || { ...fieldsForBrowse, display_name: null }

    const result = await this.web_search_read({
      ...kwargs,
      fields: fields2,
      limit: 1
    })

    // console.log(result)
    const records =
      result.length && result.records.length ? result.records[0] : {}

    if (!callback) {
      return records
    }

    // const fields3 = Object.keys(records)
    // const { columnsForPage: cols = {} } = this.metadata
    // for (let i = 0; i < fields3.length; i++) {
    //   //
    //   const fld = fields3[i]
    //   const val = records[fld]
    //   const meta = cols[fld] || {}
    //   // console.log('table----xxxx, ', fld, meta)
    //   if (meta.type === 'table' && meta.odooModel) {
    //     //
    //     console.log('table----xxxx, ', fld, val)

    //     const line_res = await this.env(meta.odooModel).web_search_read({
    //       domain: { id: val },
    //       context
    //     })
    //     console.log('table----xxxx, ', fld, val, line_res)
    //     const records2 = { ...records, [`${fld}__object`]: line_res.records }
    //     callback(records2)
    //   }
    // }

    return records

    // callback(records)
  }

  // 参数 id, {fields}
  async browse_one(id, query = {}) {
    const res = await this.browse(id, query)
    return res && res.length === 1 ? res[0] : {}
  }

  // 参数 id or ids, {fields}
  async browse(rid, query = {}) {
    const { fields, isEdit, ...query2 } = query
    const method = 'read2'

    // const { fieldsForBrowse = {}, fieldsForEdit = {} } = this.metadata || {}
    const { fieldsForBrowse = {} } = this.metadata || {}
    // const fields1 = isEdit ? fieldsForEdit : fieldsForBrowse
    // const fields1 = isEdit ? fieldsForBrowse : fieldsForBrowse
    const fields1 = isEdit ? fieldsForBrowse : fieldsForBrowse
    const fields2 = fields || { display_name: null, ...fields1 }

    // 判断 id 是数组或 int
    const args = [
      Array.isArray(rid) ? rid.map(item => parseInt(item)) : parseInt(rid)
    ]
    const kwargs = { ...query2, fields: fields2 }

    // read2 返回值是 数组
    const records = await this.call(method, args, kwargs)

    // console.log('read2 res', records)

    const records2 = records.map(item => {
      const image_fields = this.get_record(item)
      const item2 = { ...item, ...image_fields }
      return item2
    })
    // console.log('read2 res', records2)

    return records2
  }

  // 参数 id
  async unlink(rid) {
    const method = 'unlink'
    const args = [rid]
    return await this.call(method, args)
  }

  // 参数 values, {fields}
  async create(values = {}, kwargs = {}) {
    const method = 'create2'
    const args = [values]
    const rec = await this.call(method, args, kwargs)
    const image_fields = this.get_record(rec)
    const rec2 = { ...rec, ...image_fields }
    return rec2
  }

  // 参数 rid, values, {fields}
  async write(rid, values, kwargs = {}) {
    // console.log('xxxxx, write', values)
    const method = 'write2'
    const args = [rid, values]
    const rec = await this.call(method, args, kwargs)
    const image_fields = this.get_record(rec)
    const rec2 = { ...rec, ...image_fields }
    return rec2
  }

  async search_or_create(kwargs = {}) {
    const { domain, values, context } = kwargs
    const rec = await this.search_one({ domain, context })
    if (rec.id) {
      return rec
    } else {
      return this.create(values, { context })
    }
  }

  // 更新或创建
  async update_or_create(values, kwargs = {}) {
    console.log('update_or_create, ', values, kwargs)
    const { domain, fields } = kwargs

    if (domain) {
      const rec = await this.search_one({ domain })
      if (rec.id) {
        return this.write({ id: rec.id, ...values }, { fields })
      } else {
        return this.create(values, { fields })
      }
    } else {
      return this.write(values, kwargs)
    }
  }
}

class ModelClassNew extends ModelClassCrud {
  get_globals_dict({ /*values,  */ globals_dict = {} }) {
    // 全部 odoo 只有这4个 模型 在获取 fields_get时, 需要提供 globals_dict, 设置 domain
    // 其余的只是需要 company_id
    // --- res.partner
    // <-str---> state_id [('country_id', '=?', country_id)]

    // --- sale.order.line
    // <-str---> product_uom [('category_id', '=', product_uom_category_id)]

    // --- purchase.order.line
    // <-str---> product_uom [('category_id', '=', product_uom_category_id)]

    // --- stock.move
    // <-str---> product_uom [('category_id', '=', product_uom_category_id)]

    // return { ...globals_dict, company_id: this.get_company_id()() }
    // 服务端 补充 company_id, 这里不需要了.
    // 这个函数 目前只用于 被 override 补充字段
    return globals_dict
  }

  // 搜索条件 的 select options 需要补充额外的 domain
  // TBD 暂时没处理 2020-11-2
  async fields_get(payload2 = {}) {
    const {
      fields: fields_src,
      globals_dict = {},
      page,
      callback,
      checkcb,
      ...payload
    } = payload2

    const get_fields = () => {
      const metadata = this.metadata || {}
      const { columnsForFilter = {}, fieldsForBrowse = {} } = metadata
      if (fields_src) {
        return fields_src
      } else if (page === 'list') {
        return Object.keys(columnsForFilter)
      } else if (page === 'o2mEdit') {
        // POL SOL MO 三个 模型 的 product_uom 字段 的 额外补丁
        return Object.keys(fieldsForBrowse).filter(
          item => (fieldsForBrowse[item] || {}).forO2mEditFieldsGet
        )
      } else if (page === 'test') {
        return []
      } else {
        return Object.keys(fieldsForBrowse)
      }
    }

    const fields = get_fields()

    if (!fields.length && page !== 'test') {
      return {}
    }

    const globals_dict2 = this.get_globals_dict({ globals_dict })

    // const { allfields, attributes } = kwargs || {}
    const method = 'fields_get2'
    const args = [
      fields,
      ['type', 'selection', 'relation', 'domain', 'readonly']
    ]
    const res = await this.call(method, args, {
      globals_dict: globals_dict2,
      ...payload
    })

    // console.log('xxx, in models, fields_get,', this.model, res)
    const res2 = Object.keys(res).reduce((acc, cur) => {
      let item = {}
      if (res[cur].type === 'selection') {
        // item = { ...res[cur], options: res[cur].selection }
        item = res[cur]
      } else {
        item = res[cur]
      }
      acc[cur] = item

      return acc
    }, {})

    if (callback) {
      this._after_onchange({ fields: res2, callback, checkcb })
    }
    return res2
  }

  dataDict_to_formData(values, fieldsGet) {
    // 这个函数 只适用于 onchange 的返回值
    // console.log('dataDict_to_formData, 000, ', this.model, values, fieldsGet)
    const values2 = Object.keys(values).reduce((acc, cur) => {
      if (cur.split('__').length > 1 && cur.split('__')[0]) {
        return acc
      }
      const meta = fieldsGet[cur] || {}

      if (['many2one', 'selection'].includes(meta.type)) {
        acc[cur] = values[cur]
      } else if (['many2many', 'one2many'].includes(meta.type)) {
        // 这是 onchange 返回值
        const o2m_value = values[cur].map(item => {
          if (![0, 1].includes(item[0])) {
            return item
          }
          if (!meta.fields) {
            return item
          }
          const [op, rid, vals] = item
          // console.log('dataDict_to_formData, o2m,', item, meta.fields)
          return [op, rid, this.dataDict_to_formData(vals, meta.fields)]
        })
        // console.log('dataDict_to_formData, o2m ret,', cur, o2m_value)
        acc[cur] = o2m_value
      } else {
        acc[cur] = values[cur]
      }

      return acc
    }, {})
    // console.log('dataDict_to_formData,2 ', values2)

    return values2
  }

  dataDict_to_formData2(values, fieldsGet, fieldsGetOld = {}) {
    // 这个函数 只适用于 onchange 的返回值
    // console.log('dataDict_to_formData, 000, ', this.model, values, fieldsGet)
    const values2 = Object.keys(values).reduce((acc, cur) => {
      const meta1 = fieldsGet[cur] || {}
      const meta2 = fieldsGetOld[cur] || {}
      const meta = { ...meta2, ...meta1 }
      // console.log('dataDict_to_formData2, ', this.model, cur, meta)

      if (!meta.readonly) {
        acc[cur] = values[cur]
      }

      return acc
    }, {})
    // console.log('dataDict_to_formData,2 ', values2)

    return values2
  }

  async default_get_onchange(payload = {}) {
    const { values = {} } = payload
    return this.onchange({ ids: [], values, field_name: [], ...payload })
  }

  async onchange(payload = {}) {
    // console.log('onchange,', this.model, payload)
    // console.log('onchange,metadata，', this.model, this.metadata)

    // values 缺少其他默认值, 也影响 onchange 返回值
    const {
      ids = [],
      values = {},
      field_name = [],
      context: context_src,
      fieldsGet,
      callback,
      checkcb,
      ...payload2
    } = payload

    const {
      field_onchange,
      field_onchange_ref,
      field_onchange_model,
      field_onchange_field
    } = this.metadata || {}

    const context = context_src

    const method = 'onchange2'
    const args = [ids, values, field_name, field_onchange]
    const kwargs = {
      ...payload2,
      field_onchange_ref,
      field_onchange_model,
      field_onchange_field,
      context
    }
    const result = await this.call(method, args, kwargs)

    const { value, fields, domain } = result
    const value_form = this.dataDict_to_formData(value, fields, fieldsGet)
    const value_form2 = this.dataDict_to_formData2(
      value_form,
      fields,
      fieldsGet
    )

    if (callback) {
      this._after_onchange({ fields, domain, callback, checkcb })
    }

    // console.log(
    //   'onchange result,',
    //   this.model,
    //   fields,
    //   value,
    //   value_form,
    //   value_form2
    // )
    return { ...result, value_form, value_form2 }

    // return {
    //   ...result,
    //   value: JSON.parse(JSON.stringify(value)),
    //   value_form: JSON.parse(JSON.stringify(value_form))
    // }
  }

  _after_onchange(payload) {
    const { fields: result, domain: domains = {}, callback, checkcb } = payload
    // console.log('_after_onchange', result, domains)
    const flds = Object.keys(this.metadata.fieldsForBrowse)
    Object.keys(result).forEach(item => {
      const meta = result[item]
      // if (meta.type === 'many2one') {
      //   console.log('fg, many2one', item, meta.domain)
      // }

      const isToCall =
        meta.type === 'many2one' &&
        !(checkcb && checkcb(item)) &&
        Array.isArray(meta.domain) &&
        flds.includes(item)

      if (isToCall) {
        // console.log('fgs', item, meta, domains[item] || [])
        const domain = [...(domains[item] || []), ...meta.domain]
        const rel = this.env(meta.relation)
        const res2 = rel.name_search({ domain })
        res2.then(res => {
          // console.log('fgs', item, domain, res)
          callback(item, res)
        })
      }
    })
  }
}

class ModelClassWorkflow extends ModelClassNew {
  async workflow2(rid, method, kwargs = {}) {
    // console.log('xxxxx, workflow2', rid, method, kwargs)
    // const call_res =
    await this[method](rid, kwargs)
    // console.log('xxxxx, workflow2 2,', rid, method, call_res)
    const record = await this.browse_one(rid)
    return { record }
  }
  async workflow(rid, method, dataDict) {
    const meta = this.metadata.workflow.buttons[method]
    if (meta.type === 'object') {
      if (this[method]) {
        await this[method](rid)
      } else {
        await this.call(method, [rid])
      }
      const record = await this.browse_one(rid)
      return { record }
    } else if (meta.type === 'action') {
      //
      return await this.workflow_wizard(rid, method)
      // console.log('xxxxx, workflow', rid, method, meta)
    } else if (meta.type === 'object_action') {
      // console.log('onclick, onObjectAction, ok,', rid, method, meta)
      const action = await this.call(method, [rid], {
        context: meta.context || {}
      })
      console.log('xxxxx,object_action,', action)

      const context = { ...(meta.context || {}), ...action.context }
      console.log('xxxxx,object_action,', context)

      // const context = {
      //   ...meta.context,
      //   active_id: this.dataDict.id,
      //   active_ids: [this.dataDict.id],
      //   active_model: this.metadata.model,
      //   ...action.context
      // }

      return {
        action: {
          ...action,
          context,
          path: meta.path,
          query: { context: JSON.stringify(context) }
        }
      }
    } else if (meta.type === 'router') {
      // console.log('onclick, onObjectAction, ok,', rid, method, meta)
      const { path, domain, string } = meta
      const domain2 =
        domain && typeof domain === 'function' ? domain(dataDict) : domain

      const query = { title: string, domain: JSON.stringify(domain2) }
      return { action: { path, query } }
    } else {
      return {}
    }
  }

  async workflow_wizard(rid, method, kwargs = {}) {
    //
    const meta = this.metadata.workflow.buttons[method].wizard
    if (meta.submit) {
      const model = meta.model
      const record = await this.wizard_submit(rid, model, meta.method, kwargs)
      return { record }
    } else {
      //
      console.log('I am To Be Done')
      return {}
    }
  }

  async wizard_submit(rid, model, method, kwargs) {
    const result = await this.wizard_default_get_onchange(rid, model, kwargs)
    const values = this.dataDict_to_formData(result.value, result.fields)
    const wizid = await this.wizard_create(rid, model, values)
    // const res2 = await this.wizard_call(rid, model, 'read', [wizid])

    // const res1 =
    await this.wizard_call(rid, model, method, [wizid], kwargs)
    const record = await this.browse_one(rid, kwargs)
    // console.log('wizard_call, ', values, wizid, res1, record)
    return record
  }

  async wizard_call(rid, model, method, args, kwargs = {}) {
    const { context: context_src = {}, toCall, ...kwargs2 } = kwargs
    const wizard_Model = this.env(model)
    const context = {
      ...context_src,
      active_id: rid,
      active_ids: [rid],
      active_model: this.model
    }

    if (toCall) {
      return wizard_Model[method](...args, { ...kwargs2, context })
    } else {
      return wizard_Model.call(method, args, { ...kwargs2, context })
    }
  }

  async wizard_default_get_onchange(rid, model, kwargs = {}) {
    const method = 'default_get_onchange'
    const result = await this.wizard_call(rid, model, method, [], {
      ...kwargs,
      toCall: 1
    })
    // console.log('wizard_default_get_onchange', result)
    return result
  }

  async wizard_create(rid, model, values, kwargs = {}) {
    const result = await this.wizard_call(
      rid,
      model,
      'create',
      [values],
      kwargs
    )
    // console.log('wizard_create', result)
    return result
  }
}

class ModelClassImportExport extends ModelClassWorkflow {
  async import_data(payload) {
    //
    console.log('import data', payload)
    const { file, context: contextSrc = {} } = payload || {}

    const { context: contextForDefault = {}, import_export = {} } =
      this.metadata || {}

    const { fields, name_create_enabled_fields = {} } = import_export
    const columns = fields
    const import_obj = this.env('base_import.import')
    const context = { ...contextForDefault, ...contextSrc }

    const options = {
      headers: true,
      advanced: false,
      keep_matches: false,
      name_create_enabled_fields,
      skip: 0,
      limit: 2000,
      encoding: '',
      separator: '',
      quoting: '"',
      date_format: '%Y-%m-%d',
      datetime_format: '%Y-%m-%d %H:%M:%S',
      float_thousand_separator: ',',
      float_decimal_separator: '.',
      fields: []
    }

    const values = { res_model: this.model }
    const import_id = await import_obj.call('create', [values], { context })
    console.log(' import import_id', import_id)

    const wr_res = await this.rpc.base_import_set_file({ import_id, file })
    console.log(' import base_import_set_file', wr_res)

    const res = await import_obj.call(
      'do',
      [import_id, fields, columns, options],
      { context }
    )
    console.log(' import do', res)

    return res
  }
}

class ModelClassReport extends ModelClassImportExport {
  async report_month(payload = {}) {
    const { date_month } = payload
    const result = await this.default_get_onchange()
    // console.log('values_init', result)
    const values_init = this.dataDict_to_formData(result.value, result.fields)
    // console.log('values_init', values_init)
    const str00 = value => value.toString().padStart(2, '0')
    const date2str = date => {
      const yyyy = str00(date.getFullYear())
      const mm = str00(date.getMonth() + 1)
      const dd = str00(date.getDate())
      return `${yyyy}-${mm}-${dd}`
    }

    const first_date = date => new Date(date.getFullYear(), date.getMonth(), 1)
    const date_add = (date, day) =>
      new Date(date.getTime() + 1000 * 60 * 60 * 24 * day)

    const get_date = date => {
      const date1 = new Date(date)
      const this_first = first_date(date1)
      const this_last = date_add(first_date(date_add(this_first, 40)), -1)
      const date_from = date2str(this_first)
      const date_to = date2str(this_last)

      return [date_from, date_to]
    }

    const [date_from, date_to] = get_date(date_month)

    // console.log('dataetteete, ', date_month, date_from, date_to)
    const values = { ...values_init, date_from, date_to }
    const context = { report_type: 'month' }

    const res = this.report_print({ values, context })
    return res
  }

  async report_print(payload = {}) {
    const { values, context = {} } = payload
    const method = 'report_print'
    const args = [{ ...values }]
    const payload2 = { ...payload }
    delete payload2.values
    const kwargs = { ...payload2 }
    const res = await this.call(method, args, kwargs)
    const { report_type = 'date' } = context

    const res2 = { ...res, context: { ...context, report_type } }
    return res2
  }
}

export class ModelClass extends ModelClassReport {}

const creater = options => {
  return new ModelClass(options)
}
export default creater
