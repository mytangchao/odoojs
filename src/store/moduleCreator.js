import api from '@/api'

const creator = (config, module = {}) => {
  // console.log('module src: ', module)
  // 通过 config 或 model name,  创建 store
  const metadata = typeof config === 'string' ? { model: config } : config
  // console.log('config src: ', _config)

  const { model } = metadata

  const {
    namespaced = true,
    state: state_extend = {},
    mutations: mutations_extend = {},
    actions: actions_extend = {}
  } = module

  const { state = {}, mutations = {}, actions = {} } = api.vuex_store

  const state2 = Object.keys(state).reduce((acc, cur) => {
    const item = state[cur]
    if (Array.isArray(item)) {
      acc[cur] = [...item]
    } else if (typeof item === 'object') {
      acc[cur] = { ...item }
    } else {
      acc[cur] = item
    }
    return acc
  }, {})

  return {
    namespaced,
    state: { model, metadata, ...state2, ...state_extend },
    mutations: { ...mutations, ...mutations_extend },
    actions: { ...actions, ...actions_extend }
  }
}

export default creator
